#pragma once
namespace Fleets {

enum FleetsColumns {
  fc_Name = 0,
  fc_Leader,
  fc_Status,
  fc_Speed,
  fc_Position,
  fc_FirstOrder
};

}
