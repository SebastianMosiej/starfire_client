#include <data/generate_hex_path.h>
#include <cmath>
#include <QByteArray>

class GenerateHexPathPrivate {
 public:
  GenerateHexPathPrivate(GenerateHexPath *_pub);
  QPainterPath prepare_path();
  QByteArray GenerateSVG();

 public:
  GenerateHexPath *pub;
  unsigned int side_length;
  std::vector<QPointF> hex_points;
  qreal width;
};

GenerateHexPathPrivate::GenerateHexPathPrivate(GenerateHexPath *_pub)
    : pub(_pub) {}

QPainterPath GenerateHexPathPrivate::prepare_path()
{
  QPolygonF pol;
  width = ceil(side_length * sqrt(3)/2);
  qreal small_heigth = ceil(side_length / 2);
  hex_points.clear();
  pol << QPointF(0, small_heigth);
  hex_points.push_back(QPointF(0, small_heigth));
  pol << QPointF(width, 0);
  hex_points.push_back(QPointF(width, 0));
  pol << QPointF(width*2, small_heigth);
  hex_points.push_back(QPointF(width*2, small_heigth));
  pol << QPointF(width*2, small_heigth*3);
  hex_points.push_back(QPointF(width*2, small_heigth*3));
  pol << QPointF(width, 4 * small_heigth);
  hex_points.push_back(QPointF(width, 4 * small_heigth));
  pol << QPointF(0, small_heigth * 3);
  hex_points.push_back(QPointF(0, small_heigth * 3));
  pol << QPointF(0, small_heigth);
  hex_points.push_back(QPointF(0, small_heigth));
  QPainterPath path;
  path.addPolygon(pol);
  return path;
}

QByteArray GenerateHexPathPrivate::GenerateSVG()
{
  QByteArray result("<?xml version=\"1.0\" standalone=\"no\"?>\n");
  result+=QString("<svg width=\"%1\" height=\"%2\" xmlns=\"http://www.w3.org/2000/svg\" version=\"1.2\" baseProfile=\"tiny\">");
//                  " viewBox="0 0 1000 1000"
  QString polyline("<polyline fill=\"none\" stroke=\"black\" stroke-width=\"2\" points=\"");
  for(QPointF pos : hex_points)
  {
    polyline += QString("%1, %2 ").arg(pos.x()).arg(pos.y());
  }
  polyline += "\" /></svg>";
  result+=polyline.toLatin1();
  return result;
}

//////////////////////////////////////////////////////
GenerateHexPath::GenerateHexPath()
    : pimpl(new GenerateHexPathPrivate(this)) {}

GenerateHexPath::~GenerateHexPath() {}
void GenerateHexPath::SideLength(unsigned int length) {
  pimpl->side_length = length;
}

unsigned int GenerateHexPath::SideLength() { return pimpl->side_length; }
QPainterPath GenerateHexPath::generate()
{
  return pimpl->prepare_path();
}
QPixmap GenerateHexPath::generate_pixmap()
{
//  QScopedPointer<QGraphicsSvgItem> svgItem(new QGraphicsSvgItem(fileName));
//  if (!svgItem->renderer()->isValid())
//    return false;

  return QPixmap();
}
