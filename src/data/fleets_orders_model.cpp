#include <data/fleets_orders_model.h>
#include <qcolor.h>
#include <QSize>
#include <data/fleets_orders_columns.hpp>

namespace Fleets
{
namespace
{
int cOrdersCellWidth(120);
int cNameCellWidth(150);
int cSpeedCellWidth(40);
int cStatusCellWidth(100);
int cLeaderCellWidth(100);
int cPositionCellWidth(100);
}

class FleetsOrdersModelPrivate
{
  public:
  FleetsOrdersModelPrivate(FleetsOrdersModel *);
  QVariant getFleetNameCellData(const FleetOrders &fleet, int role) const;
  QVariant getSpeeed(const FleetOrders &fleet, int role) const;
  QVariant getStatus(const FleetOrders &fleet, int role) const;
  QVariant getLeader(const FleetOrders &fleet, int role) const;
  QVariant getPosition(const FleetOrders &fleet, int role) const;
  QVariant getOrdersCellData(unsigned int orderNumber, const FleetOrders &fleet, int role) const;

  public:
  FleetsOrdersModel *      pub;
  std::vector<FleetOrders> fleets;
};

FleetsOrdersModelPrivate::FleetsOrdersModelPrivate(FleetsOrdersModel *_pub) : pub(_pub) {}
QVariant FleetsOrdersModelPrivate::getFleetNameCellData(const FleetOrders &fleet, int role) const
{
  if (role == Qt::DisplayRole)
    return QVariant(fleet.name.c_str());
  else if (role == Qt::TextAlignmentRole)
  {
    return QVariant(Qt::AlignHCenter | Qt::AlignVCenter);
  }
  else if (role == Qt::SizeHintRole)
    return QSize(cNameCellWidth, 0);
  return QVariant();
}
QVariant FleetsOrdersModelPrivate::getOrdersCellData(unsigned int orderNumber, const FleetOrders &fleet, int role) const
{
  if (role == Qt::DisplayRole && fleet.orders.size() > orderNumber)
  {
    return QVariant(fleet.orders[orderNumber].c_str());
  }
  else if (role == Qt::BackgroundColorRole && fleet.orders.size() <= orderNumber)
  {
    QVariant v = QColor(Qt::gray);
    return v;
  }
  else if (role == Qt::SizeHintRole)
  {
    return QVariant(QSize(cOrdersCellWidth, 0));
  }
  return QVariant();
}
QVariant FleetsOrdersModelPrivate::getSpeeed(const FleetOrders &fleet, int role) const
{
  if (role == Qt::DisplayRole)
    return QVariant(fleet.speed);
  else if (role == Qt::SizeHintRole)
    return QSize(cSpeedCellWidth, 0);
  else
    return QVariant();
}
QVariant FleetsOrdersModelPrivate::getStatus(const FleetOrders &fleet, int role) const
{
  if (role == Qt::DisplayRole)
    return QVariant(fleet.status.c_str());
  else if (role == Qt::SizeHintRole)
    return QSize(cStatusCellWidth, 0);
  else
    return QVariant();
}
QVariant FleetsOrdersModelPrivate::getLeader(const FleetOrders &fleet, int role) const
{
  if (role == Qt::DisplayRole)
    return QVariant(fleet.leaderName.c_str());
  else if (role == Qt::SizeHintRole)
    return QSize(cLeaderCellWidth, 0);
  else
    return QVariant();
}
QVariant FleetsOrdersModelPrivate::getPosition(const FleetOrders &fleet, int role) const
{
  if (role == Qt::DisplayRole)
    return QVariant(fleet.currentPosition.c_str());
  else if (role == Qt::SizeHintRole)
    return QSize(cPositionCellWidth, 0);
  else
    return QVariant();
}

FleetsOrdersModel::FleetsOrdersModel() : pimpl(new FleetsOrdersModelPrivate(this)) {}
FleetsOrdersModel::~FleetsOrdersModel() {}
int FleetsOrdersModel::rowCount(const QModelIndex &) const { return static_cast<int>(pimpl->fleets.size()); }
int FleetsOrdersModel::columnCount(const QModelIndex &) const
{
  int count = 0;
  for (const FleetOrders &item : pimpl->fleets)
  {
    if (static_cast<int>(item.orders.size()) > count) count = static_cast<int>(item.orders.size());
  }
  return fc_FirstOrder + count + 1;
}
QVariant FleetsOrdersModel::data(const QModelIndex &index, int role) const
{
  if (!index.isValid()) return QVariant();
  if (static_cast<unsigned int>(index.row()) >= pimpl->fleets.size()) return QVariant();

  FleetOrders &fleet = pimpl->fleets[static_cast<size_t>(index.row())];
  if (role == Qt::TextAlignmentRole) return QVariant(Qt::AlignHCenter | Qt::AlignVCenter);
  switch (index.column())
  {
    case fc_Name:
      return pimpl->getFleetNameCellData(fleet, role);
    case fc_Leader:
      return pimpl->getLeader(fleet, role);
    case fc_Status:
      return pimpl->getStatus(fleet, role);
    case fc_Speed:
      return pimpl->getSpeeed(fleet, role);
    case fc_Position:
      return pimpl->getPosition(fleet, role);
    case fc_FirstOrder:
    case fc_FirstOrder + 1:
    case fc_FirstOrder + 2:
    case fc_FirstOrder + 3:
    case fc_FirstOrder + 4:
    case fc_FirstOrder + 5:
    case fc_FirstOrder + 6:
    case fc_FirstOrder + 7:
    case fc_FirstOrder + 8:
      return pimpl->getOrdersCellData(static_cast<unsigned int>(index.column() - fc_FirstOrder), fleet, role);
  }
  return QVariant();
}

QVariant FleetsOrdersModel::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (role != Qt::DisplayRole) return QVariant();
  if (orientation == Qt::Vertical) return QVariant();
  switch (section)
  {
    case fc_Name:
      return "Name";
    case fc_Leader:
      return "Leader";
    case fc_Status:
      return "Status";
    case fc_Speed:
      return "Speed";
    case fc_Position:
      return "Position";
    case fc_FirstOrder:
    case fc_FirstOrder + 1:
    case fc_FirstOrder + 2:
    case fc_FirstOrder + 3:
    case fc_FirstOrder + 4:
    case fc_FirstOrder + 5:
    case fc_FirstOrder + 6:
    case fc_FirstOrder + 7:
    case fc_FirstOrder + 8:
      return QString("StMP %1").arg(section - 4);
  }
  return QVariant();
}

void FleetsOrdersModel::addFleetDetails(FleetOrders fleet)
{
  int column = columnCount(QModelIndex());
  if (pimpl->fleets.size())
  {
    int  rows = rowCount();
    emit beginInsertRows(QModelIndex(), qMin(rows - 1, 0), qMin(rows - 1, 0));
  }
  else
    emit beginInsertRows(QModelIndex(), 0, 0);
  int    diff = static_cast<int>(fleet.orders.size()) - (column - fc_FirstOrder);
  pimpl->fleets.push_back(fleet);
  emit endInsertRows();
  if (diff > 0)
  {
    emit beginInsertColumns(QModelIndex(), column, column + diff);
    emit endInsertColumns();
  }
}
void FleetsOrdersModel::setProperties(boost::property_tree::ptree properties)
{
  cNameCellWidth = properties.get<int>("columns_width.name");
  cLeaderCellWidth = properties.get<int>("columns_width.leader");
  cStatusCellWidth = properties.get<int>("columns_width.status");
  cSpeedCellWidth = properties.get<int>("columns_width.speed");
  cPositionCellWidth = properties.get<int>("columns_width.position");
  cOrdersCellWidth = properties.get<int>("columns_width.orders");
}
}
