#ifndef SHIP_PROJECT_HPP
#define SHIP_PROJECT_HPP
#include <string>

namespace Ships
{

struct ShipProject
{
  std::string className;
  std::string hullType;
  unsigned int hullSize;
  float cost;
  float maintenance;
  std::string eqList;
  unsigned int hit_to_kill;
  std::string datalink;
  unsigned int multitarget;
  unsigned int surveyPoints;
  std::string sensorType;
  unsigned int cargoCapacity;
  unsigned int peopleCapacity;
  unsigned int lifeSupport;
  unsigned int magazineCapacity;
  unsigned int transitModifier;
  unsigned int turnMode;
  unsigned int strategicSpeed;
  unsigned int tacticalSpeed;
  std::string design;
};

}

#endif //SHIP_PROJECT_HPP
