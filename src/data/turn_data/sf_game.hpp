#ifndef SF_GAME_DATA_HPP
#define SF_GAME_DATA_HPP
#include "fleet_detail.hpp"
#include "ship_detail.hpp"
#include "ship_project.hpp"
#include "planet_detail.hpp"
#include <vector>
#include <map>

struct SFGameData
{
  std::map<std::string, FleetOrders> fleetsOrders;
  std::map<std::string, std::vector<Ships::ShipDetails>> fleetsComposition;
  std::map<std::string, Ships::ShipProject> shipProjects;
//  std::map<std::string, Planets> planets;
};
#endif //SF_GAME_DATA_HPP
