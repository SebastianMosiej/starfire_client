#ifndef SHIP_DETAILS_HPP
#define SHIP_DETAILS_HPP
#include <string>
#include <vector>
#include <map>

namespace Ships
{

enum BaseGrade
{
  bg_Poor = -2,
  bg_Green = -1,
  bg_Average = 0,
  bg_Crack,
  bg_Elite,
  bg_Ultra,
  bg_Heroic,
  bg_Legendary
};

struct ShipDetails
{
  std::string  className;
  unsigned int amount;
  std::map<BaseGrade, unsigned int> crewGrade;
  unsigned int speedTactical;
  unsigned int speedStategic;
  unsigned int levelOfDetection;
  unsigned int levelCloak;
  unsigned int maintenence;
  bool         mothball;
  std::string  notes;
};
}
#endif
