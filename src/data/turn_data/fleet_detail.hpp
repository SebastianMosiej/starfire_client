#ifndef FLEET_DETAILS_HPP
#define FLEET_DETAILS_HPP
#include <string>
#include <vector>
#include <data/turn_data/ship_detail.hpp>

struct FleetOrders
{
  std::string name;
  std::string leaderName;
  std::string status;
  std::string currentPosition;
  unsigned int speed;
  //list of orders
  std::vector<std::string> orders;
};

struct Fleet
{
  std::string name;
  std::vector<Ships::ShipDetails> ships;
};

#endif
