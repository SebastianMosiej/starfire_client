#ifndef PLANETS_DETAILS_HPP
#define PLANETS_DETAILS_HPP
#include <string>
#include <enum.h>
#include <array>


namespace Planet
{
  BETTER_ENUM(WorldType, int, Bening = 0, Harsh, Hostile, Desolate, Extreme, Asteroid, HGT, None);

  BETTER_ENUM(BodyType, int, AstBelt = 0, HGT, T, mT, ST, B, mB, H, mH, F, mF, I, G, V, mV, None);

  BETTER_ENUM(BuildingsType, int, ScienceAcademy = 0, ResearchDevelopmentStation, PilotTrainingFacility, QtTrainingFacility, PlanetaryScienceFacility, TradeHub, ImperialCommandCenter, ImperialCapital, Count);

  struct Planets
  {
    Planets() : habitability(WorldType::None)
            , bodyType(BodyType::None)
            , starNumber(-1)
            , moonNumber(-1)
            , industrialUnit(0)
            , resourceExploitationIndex(0)
    {
      buildings.fill(0);
    }
    std::string  systemName;
    unsigned int planetNumber;
    // for multistar systems -1 means one star
    int          starNumber;
    int          moonNumber;
    WorldType    habitability;
    BodyType     bodyType;
    double       population;
    double       growth;
    int          maximumPopulation;
    unsigned int industrialUnit;
    // Resource Exploitation Index
    unsigned int resourceExploitationIndex;
    double       income;
    std::array<unsigned int, BuildingsType::Count> buildings;
  };
}
#endif  // PLANETS_DETAILS_HPP
