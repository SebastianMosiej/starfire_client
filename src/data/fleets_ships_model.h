#include <memory>
#include <QAbstractTableModel>
#include <data/turn_data/ship_detail.hpp>

namespace Fleets
{

class FleetsShipsModelPrivate;

class FleetsShipsModel : public QAbstractItemModel
{
  Q_OBJECT
public:
  FleetsShipsModel();
  ~FleetsShipsModel();
  FleetsShipsModel(const FleetsShipsModel&) = delete;
  FleetsShipsModel operator=(const FleetsShipsModel&) = delete;
  
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant data(const QModelIndex &index, int role) const override;
  QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
  QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
  QModelIndex parent(const QModelIndex &child) const override;

  void addFleet(const std::string & name);
  void addShipToFleet(const std::string &fleetName, Ships::ShipDetails ship);
  void addShip(Ships::ShipDetails ship, const std::string &fleetName = std::string());

private:
  friend class FleetsShipsModelPrivate;
  std::unique_ptr<FleetsShipsModelPrivate> pimpl;
};

}
