#pragma once
#include <QObject>
#include <memory>
#include <boost/property_tree/ptree.hpp>

class DataThreadPrivate;

class DataThread : public QObject
{
  Q_OBJECT
public:
    using ptr = std::shared_ptr<DataThread>;

    DataThread();
    ~DataThread();

    boost::property_tree::ptree GetSettings();

signals:
    void propertiesLoaded();
private:
    friend class DataThreadPrivate;
    std::unique_ptr<DataThreadPrivate> _pimpl;
};
