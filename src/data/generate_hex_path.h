#ifndef GENERATE_HEX_PATH_H
#define GENERATE_HEX_PATH_H
#include <QtCore/QObject>
#include <QtGui/QPainterPath>
#include <memory>
#include <QtGui/QPixmap>

class GenerateHexPathPrivate;

class GenerateHexPath : public QObject
{
    Q_OBJECT
public:
    GenerateHexPath();
    virtual ~GenerateHexPath();
    void SideLength(unsigned int length);
    unsigned int SideLength();
    QPainterPath generate();
    QPixmap generate_pixmap();
private:
    std::unique_ptr<GenerateHexPathPrivate> pimpl;
};

#endif /*GENERATE_HEX_PATH_H*/
