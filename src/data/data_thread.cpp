#include <data/data_thread.h>
#include <logging_base.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <fstream>
#include <iostream>
#include <QDir>

namespace {
  const QString settings_file("settings.xml");  // xml file path and name
}

class DataThreadPrivate : public LoggingBase {
 public:
  DataThreadPrivate(DataThread *pub);
  void LoadSettings(QString file = settings_file);

 public:
  DataThread *_pub;
  boost::property_tree::ptree settings_tree;
};

DataThreadPrivate::DataThreadPrivate(DataThread *pub) : _pub(pub) {
  LoadSettings();
  logger.log(log4cplus::INFO_LOG_LEVEL, "Data thread created");
}

void DataThreadPrivate::LoadSettings(QString file) {
  if (false == QFile::exists(file))
  {
    logger.log(log4cplus::ERROR_LOG_LEVEL, QString("Lack of properties file (%1)").arg(file).toStdString().c_str());
    return;
  }
  logger.log(log4cplus::DEBUG_LOG_LEVEL,
             QString("Loading properties file (%1)").arg(file).toStdString());
  using boost::property_tree::ptree;
  try {
    std::basic_ifstream<char> stream(file.toLatin1());
    read_xml(stream, settings_tree);
  } catch (boost::property_tree::xml_parser::xml_parser_error const & ex) {
    logger.log(log4cplus::ERROR_LOG_LEVEL, QString("Error during properties file parsing ('%1')").arg(ex.message().c_str()).toStdString().c_str());
    return;
  }
  emit _pub->propertiesLoaded();
  logger.log(log4cplus::INFO_LOG_LEVEL,
             "Settings: loading settings from file finished");
}

////////////////////////////////////////////////////////////////////////////////
DataThread::DataThread() : _pimpl(new DataThreadPrivate(this)) {}
DataThread::~DataThread() {}

boost::property_tree::ptree DataThread::GetSettings() {
  return _pimpl->settings_tree;
}
