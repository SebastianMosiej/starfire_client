#pragma once
#include <string>
#include <vector>
#include <memory>
#include <data/hull_data.h>

namespace Data
{

class StarFireXmlLoaderPrivate;

class StarFireXmlLoader
{
public:
  StarFireXmlLoader();
  ~StarFireXmlLoader();
  bool load(const std::string &path);
  std::vector<Data::HullData> getHullData();
private:
  friend class StarFireXmlLoaderPrivate;
  std::unique_ptr<StarFireXmlLoaderPrivate> pimpl;
};

} //end of namespace Data
