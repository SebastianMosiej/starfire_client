#include <data/parser/starfire_xml_load_p.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <fstream>
#include <iostream>
//#include <QDir>

namespace Data {

StarFireXmlLoaderPrivate::StarFireXmlLoaderPrivate(StarFireXmlLoader * _pub) : pub(_pub)
{

}

void StarFireXmlLoaderPrivate::parse_fuselages(boost::property_tree::ptree &data)
{
  for(boost::property_tree::ptree::value_type &v : data.get_child("starfire_data.fuselages")) {
      Data::HullData hull;
      hull.name = v.second.get<std::string>("hull_name");
      hullData.push_back(hull);
    }
}

bool StarFireXmlLoaderPrivate::load(const std::string &file_path)
{
  hullData.clear();
  boost::property_tree::ptree game_data;
  logger.log(log4cplus::DEBUG_LOG_LEVEL,
             QString("Loading StarFire data file (%1)").arg(file_path.c_str()).toStdString());
  using boost::property_tree::ptree;
  try {
    std::basic_ifstream<char> stream(file_path);
    read_xml(stream, game_data);
  } catch (boost::property_tree::xml_parser::xml_parser_error const & ex) {
    logger.log(log4cplus::ERROR_LOG_LEVEL, QString("Error during properties file parsing ('%1')").arg(ex.message().c_str()).toStdString().c_str());
    return false;
  }
  logger.log(log4cplus::INFO_LOG_LEVEL,
             "Loading StarFire data file finished. Parsing started");
  parse_fuselages(game_data);

  logger.log(log4cplus::INFO_LOG_LEVEL,
             "Parsing StarFire data file completed.");
  return true;
}
std::vector<Data::HullData> StarFireXmlLoaderPrivate::getHullData()
{
  return  hullData;
}
}
