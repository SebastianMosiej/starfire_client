#pragma once
#include <data/turn_data/fleet_detail.hpp>
#include <memory>
#include <vector>

namespace Fleets {

class FleetsXlsxParser {
public:
  FleetsXlsxParser(const std::string &path);
  ~FleetsXlsxParser();
  std::vector<FleetOrders> get_fleet_orders();

private:
  class Private;
  friend class Private;
  std::unique_ptr<Private> pimpl;
};
}
