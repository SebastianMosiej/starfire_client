#pragma once
#include <data/parser/starfire_xml_load.hpp>
#include <boost/property_tree/ptree.hpp>
#include <logging_base.h>
#include <string>

namespace Data
{

class StarFireXmlLoaderPrivate : public LoggingBase
{
public:
  StarFireXmlLoaderPrivate(StarFireXmlLoader * _pub);

  bool load(const std::string &file_path);
  void parse_fuselages(boost::property_tree::ptree &data);
  std::vector<Data::HullData> getHullData();
public:

  StarFireXmlLoader * pub;
  std::vector<Data::HullData>    hullData;
};

} //end of namespace Data
