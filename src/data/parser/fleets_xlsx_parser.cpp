#include <data/parser/fleets_xlsx_parser.hpp>
#include <xlsxdocument.h>
#include <xlsxabstractsheet.h>
#include <xlsxworksheet.h>
#include <xlsxcell.h>

namespace Fleets {
enum Columns {
  col_FleetName = 1,
  col_Leader = 4,
  col_Status = 11,
  col_Speed = 15,
  col_FirstOrder = 17,
  col_Position = 35
};

class FleetsXlsxParser::Private {
 public:
  Private(FleetsXlsxParser *_pub, const std::string &path);
  std::vector<FleetOrders> get_fleet_orders();
  std::vector<std::string> get_orders(QXlsx::Worksheet * sheet, int row);

 public:
  FleetsXlsxParser *pub;
  QXlsx::Document xlsx;
  
};

FleetsXlsxParser::Private::Private(FleetsXlsxParser *_pub, const std::string &path)
    : pub(_pub), xlsx(path.c_str()) {}

std::vector<std::string> FleetsXlsxParser::Private::get_orders(QXlsx::Worksheet * sheet, int row)
{
  std::vector<std::string> orders;
  std::string str;
  int column = col_FirstOrder;
  for(;;)
  {
    str = sheet->cellAt(row, column)->value().toByteArray().toStdString();
    if (str.empty() || column >= col_Position)
      break;
    orders.push_back(str);
    column+=3;
  }

  return orders;
}

std::vector<FleetOrders> FleetsXlsxParser::Private::get_fleet_orders()
{
  std::vector<FleetOrders> fleetsOrders;

  QXlsx::AbstractSheet * fleetSheet =  xlsx.sheet("Fleet");
  if (!fleetSheet)
    return fleetsOrders;
  int initialRow = 8;

  if (fleetSheet->sheetType() == QXlsx::AbstractSheet::ST_WorkSheet)
  {
    QXlsx::Worksheet * sheet = dynamic_cast<QXlsx::Worksheet*>(fleetSheet);
    for (int row = initialRow;;row++)
    {
      std::string  str =  sheet->cellAt(row, col_FleetName)->value().toByteArray().toStdString();
      if (str.empty())
          break;
      FleetOrders orders;
      orders.name = str;
      orders.leaderName = sheet->cellAt(row, col_Leader)->value().toByteArray().toStdString();
      orders.status = sheet->cellAt(row, col_Status)->value().toByteArray().toStdString();
      orders.speed = sheet->cellAt(row, col_Speed)->value().toUInt();
      orders.currentPosition = sheet->cellAt(row, col_Position)->value().toByteArray().toStdString();
      orders.orders = get_orders(sheet, row);

      fleetsOrders.push_back(orders);
    }
  }

  return fleetsOrders;
}

////////////////////////////////////////////////////////////////////////////////
FleetsXlsxParser::FleetsXlsxParser(const std::string & path) : pimpl(new FleetsXlsxParser::Private(this,path)) {}

FleetsXlsxParser::~FleetsXlsxParser() {}

std::vector<FleetOrders> FleetsXlsxParser::get_fleet_orders() {
  std::vector<FleetOrders> orders(pimpl->get_fleet_orders());
  return orders;
}
}
