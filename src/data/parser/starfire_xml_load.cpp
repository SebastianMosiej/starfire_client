#include <data/parser/starfire_xml_load.hpp>
#include <data/parser/starfire_xml_load_p.hpp>
#include <QFile>

namespace Data
{

StarFireXmlLoader::StarFireXmlLoader() : pimpl(new StarFireXmlLoaderPrivate(this))
{  }

StarFireXmlLoader::~StarFireXmlLoader()
{  }

bool StarFireXmlLoader::load(const std::string &path)
{
  if (path.empty())
    return false;
  if (false == QFile::exists(path.c_str()))
    return false;
//  QFile file(path.c_str());
//  if (!file.open(QIODevice::ReadOnly))
//      return false;
  return pimpl->load(path);
}

std::vector<Data::HullData> StarFireXmlLoader::getHullData()
{
  return  pimpl->getHullData();
}

}
