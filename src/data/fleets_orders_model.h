#pragma once
#include <memory>
#include <QAbstractTableModel>
#include <data/turn_data/fleet_detail.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ptree_fwd.hpp>

namespace Fleets
{

class FleetsOrdersModelPrivate;

class FleetsOrdersModel : public QAbstractTableModel
{
  Q_OBJECT
public:
  FleetsOrdersModel();
  ~FleetsOrdersModel();

  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant data(const QModelIndex &index, int role) const override;
  QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
  void setProperties(boost::property_tree::ptree properties);

  void addFleetDetails(FleetOrders);

private:
  friend class FleetsOrdersModelPrivate;
  std::unique_ptr<FleetsOrdersModelPrivate> pimpl;
};

}
