#ifndef FUSELAGE_DATA_HPP
#define FUSELAGE_DATA_HPP
#include <utility>


struct FuselageEngineData
{
  std::string name;
  unsigned int cruise_speed;
  unsigned int turn_mode;
  bool modifier;
};

struct FuselageData
{
  std::string name;
  unsigned int freighter_nr;
  unsigned int required_el_level;
  std::pair<unsigned int, unsigned int> hs_size;
  float hs_cost;
  float engine_power;
};
//  <fuselages>
//    <fuselage>
//      <hull_name>EX</hull_name>
//      <freighter_number>0</freighter_number>
//      <req_el>1</req_el>
//      <min_hs_size>5</min_hs_size>
//      <max_hs_size>7</max_hs_size>
//      <cost_hs>3.5</cost_hs>
//      <engine_power>0.25</engine_power>
//      <speeds>
//        <speed name="Ia" cruise="6" turn_mode="2" modifier="true"/>
//        <speed name="Ja" cruise="8" turn_mode="3" modifier="true"/>
//        <speed name="Ica" cruise="4" turn_mode="2" modifier="true"/>
//        <speed name="Gta" cruise="8" turn_mode="3" modifier="true"/>
//        <speed name="Jca" cruise="14" turn_mode="8" modifier="true"/>
//      </speeds>
//      <gt_engine_power>0.125</gt_engine_power>
//      <ft_hs_cost>2.75</ft_hs_cost>
//    </fuselage>
#endif //FUSELAGE_DATA_HPP
