#include <data/fleets_ships_model.h>
#include <map>
#include <vector>

namespace Fleets
{
using FleetContent = std::vector<Ships::ShipDetails>;

class FleetsShipsModelPrivate
{
  public:
  FleetsShipsModelPrivate(FleetsShipsModel *);

  public:
  FleetsShipsModel *        pub;
  std::vector<std::string>  fleets;
//  std::vector<FleetContent> fleets;
  std::map<std::string, FleetContent> data;
};

FleetsShipsModelPrivate::FleetsShipsModelPrivate(FleetsShipsModel *_pub) : pub(_pub) {}
/////////////////////////////////////////////////////////////////////////////////////
FleetsShipsModel::FleetsShipsModel() : pimpl(new FleetsShipsModelPrivate(this)) {}
FleetsShipsModel::~FleetsShipsModel() {}
int FleetsShipsModel::rowCount(const QModelIndex &/*parent*/) const { return static_cast<int>(pimpl->data.size()); }
int FleetsShipsModel::columnCount(const QModelIndex &parent) const 
{
    if (!pimpl->data.size())
        return 0;
    if (!parent.isValid())
        return 1;
    else
        return 9;

    return 0; 
}
QVariant FleetsShipsModel::data(const QModelIndex &index, int role) const
{
  if (!index.isValid()) return QVariant();
  if (!index.parent().isValid())
  {
      int row = index.row();
      if (row <0 || row >= static_cast<int>(pimpl->fleets.size()))
          return QVariant();
      if (role == Qt::DisplayPropertyRole)
      {
          return QVariant(QString(pimpl->fleets[(size_t)row].c_str()));
      }

      // this is fleet level



  }
  else
  {

  }

  // pimpl->data

  return QVariant();
}
QVariant FleetsShipsModel::headerData(int /*section*/, Qt::Orientation /*orientation*/, int /*role*/) const { return QVariant(); }
QModelIndex FleetsShipsModel::index(int row, int column, const QModelIndex &/*parent*/) const
{
  if (row < 0 || row >= static_cast<int>(pimpl->data.size())) return QModelIndex();
  return createIndex(row, column);
}
QModelIndex FleetsShipsModel::parent(const QModelIndex &/*child*/) const { return QModelIndex(); }
void FleetsShipsModel::addFleet(const std::string &/*name*/) {}
void FleetsShipsModel::addShipToFleet(const std::string &/*fleetName*/, Ships::ShipDetails /*ship*/) {}
void FleetsShipsModel::addShip(Ships::ShipDetails ship, const std::string &fleetName)
{
  std::string fleet = fleetName;
  if (fleetName.empty())
  {
    fleet = ship.className + " Fleet";
  }
  pimpl->fleets.push_back(fleet);
  pimpl->data[fleet].push_back(ship);
}
}
