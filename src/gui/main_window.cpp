#include <data/fleets_orders_model.h>
#include <gui/main_window.h>
#include <gui/ship_designer/ship_stats_header.h>
#include <QBoxLayout>
#include <QHeaderView>
#include <gui/fleet_orders_context_menu.hpp>

//#include <gui/battle_scene.h>
//#include <gui/custom_graphics_view.h>

void MainWindow::setupUI() {
  setMinimumWidth(1200);

  QHBoxLayout *layout = new QHBoxLayout();
  QWidget *main_widget = new QWidget();
  main_widget->setLayout(layout);

  fleetView = new QTableView;
  fleetView->setModel(&fleetModel);
  fleetView->horizontalHeader()->setStretchLastSection(true);
  layout->addWidget(fleetView);

  setCentralWidget(main_widget);
  bool bResult = QObject::connect(
      &fleetModel, SIGNAL(rowsInserted(const QModelIndex &, int, int)),
      fleetView, SLOT(resizeColumnsToContents()));
  logConnection("MainWindow", "rowsInserted", bResult);
}

void MainWindow::createFleetView() {
  FleetOrders detail;
  detail.name = "1. Fleet";
  detail.status = "Patrol";
  detail.speed = 4;
  detail.orders.push_back("Fly to Laterus");
  detail.orders.push_back("Fly to Rigel");
  detail.orders.push_back("Fly to Aquile");
  detail.orders.push_back("Patrol");
  fleetModel.addFleetDetails(detail);

  FleetOrders detail2;
  detail2.name = "Warp Survey Fleet";
  detail2.status = "Warp Survey";
  detail2.speed = 4;
  detail2.orders.push_back("Fly to Aquile");
  detail2.orders.push_back("Warp Survey");
  detail2.orders.push_back("Warp Survey");
  detail2.orders.push_back("Warp Survey");
  fleetModel.addFleetDetails(detail2);
}

////////////////////////////////////////////////////
MainWindow::MainWindow(DataThread::ptr dataThread_)
    : fleetView(nullptr),
      dataThread(dataThread_),
      scene(dataThread->GetSettings()) {
  setObjectName("Main Window");
}

MainWindow::~MainWindow() {}
