#ifndef _GENERATESVGHEX_H
#define _GENERATESVGHEX_H
#include <memory>

class GenerateSVGHexPrivate;

class GenerateSVGHex
{
public:
    GenerateSVGHex();
    ~GenerateSVGHex();
protected:
    friend class GenerateSVGHexPrivate;
    std::unique_ptr<GenerateSVGHexPrivate> _pimpl;
};

#endif /* _GENERATESVGHEX_H */
