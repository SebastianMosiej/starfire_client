#include "graphics_hex_path.h"
#include <QPainterPath>
#include <QPen>
#include <cmath>

class graphics_hex_path_private {
 public:
  graphics_hex_path_private(graphics_hex_path *_pub);
  void prepare_path();
  void calculate();

 public:
  unsigned int sideLength;
  static QPainterPath hex;
  graphics_hex_path *pub;
};

graphics_hex_path_private::graphics_hex_path_private(graphics_hex_path *_pub)
    : pub(_pub) {
}
void graphics_hex_path_private::prepare_path() {
  QPolygonF pol;
  qreal width = ceil(sideLength * sqrt(3));
  qreal small_heigth = ceil(sideLength / 2);
  qreal x(0), y(small_heigth);
  pol << QPointF(x, y);
  //    hex_points.push_back(QPointF(0,y));
  x = ceil(width / 2);
  y = 0;
  pol << QPointF(x, y);
  //    hex_points.push_back(QPointF(x,0));
  x = width;
  y = small_heigth;
  pol << QPointF(x, y);
  //    hex_points.push_back(QPointF(x,y));
  x = width;
  y = small_heigth + sideLength;
  pol << QPointF(x, y);
  //    hex_points.push_back(QPointF(x,y));
  x = ceil(width / 2);
  y = 2 * sideLength;
  pol << QPointF(x, y);
  //    hex_points.push_back(QPointF(x,y));
  x = 0;
  y = small_heigth + sideLength;
  pol << QPointF(x, y);
  //    hex_points.push_back(QPointF(x,y));
  x = 0;
  y = small_heigth;
  pol << QPointF(x, y);
  //    hex_points.push_back(QPointF(x,y));
  //    hex.addPolygon(pol);
//  graphics_hex_path_private::hex.addPolygon(pol);
}

/////////////////////////////////////////////////////////////////////
graphics_hex_path::graphics_hex_path(unsigned int /*sideLength*/)
    : QGraphicsPathItem(), pimpl(new graphics_hex_path_private(this)) {
//  if (graphics_hex_path_private::hex.isEmpty()) prepare_path();
//  pub->setPath(graphics_hex_path_private::hex);
//  setPath(pimpl->hex);
  QPen pen_ = pen();
  pen_.setWidth(2);
  setPen(pen_);
}
graphics_hex_path::~graphics_hex_path() {}

//void graphics_hex_path::setSideLength(unsigned int length) {
//  pimpl->length = length;
//  pimpl->prepare_path();
//}
