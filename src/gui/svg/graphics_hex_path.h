#ifndef graphics_hex_path_H
#define graphics_hex_path_H
#include <memory>
#include <QGraphicsPathItem>

class graphics_hex_path_private;

class graphics_hex_path : public QGraphicsPathItem
{
//    Q_OBJECT
public:
    graphics_hex_path(unsigned int sideLength);
    virtual ~graphics_hex_path();
private:
    std::unique_ptr<graphics_hex_path_private> pimpl;
};

#endif /*graphics_hex_path_H*/
