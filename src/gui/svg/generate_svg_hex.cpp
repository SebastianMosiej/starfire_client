#include <gui/svg/generate_svg_hex.h>

class GenerateSVGHexPrivate
{
public:
    GenerateSVGHexPrivate(GenerateSVGHex * _pub);
public:
    GenerateSVGHex * pub;

};

GenerateSVGHexPrivate::GenerateSVGHexPrivate(GenerateSVGHex * _pub) : pub(_pub){}

////////////////////////////////////////////////////////////
GenerateSVGHex::GenerateSVGHex() : _pimpl(new GenerateSVGHexPrivate(this))
{
}

GenerateSVGHex::~GenerateSVGHex(){}
