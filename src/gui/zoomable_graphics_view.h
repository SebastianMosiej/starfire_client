#pragma once
#include <QGraphicsView>
#include <logging_base.h>
#include <QPoint>

class zoomable_graphic_view : public QGraphicsView, public LoggingBase
{
protected:
  QPoint mouse_origin_pos;
protected:
  void wheelEvent(QWheelEvent *event);
  void mousePressEvent(QMouseEvent* event);
  void mouseMoveEvent(QMouseEvent* event);
};
