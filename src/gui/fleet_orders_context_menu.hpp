#ifndef FLEET_ORDERS_CONTEXT_MENU_HPP
#define FLEET_ORDERS_CONTEXT_MENU_HPP
#include <QTableView>
#include <cstddef>
#include <logging_base.h>

namespace Fleets
{
class FleetsOrdersView : public QTableView, public LoggingBase
{
  Q_OBJECT
public:
  FleetsOrdersView(QWidget *parent = nullptr);
  FleetsOrdersView(const FleetsOrdersView&) = delete;
  FleetsOrdersView & operator=(const FleetsOrdersView&) = delete;
protected slots:
  void customMenuRequested(QPoint);
protected:
  QMenu * generateFlyToSubmenu();
  QMenu * generateContextMenu(QModelIndex idx);
};
}
#endif //FLEET_ORDERS_CONTEXT_MENU_HPP
