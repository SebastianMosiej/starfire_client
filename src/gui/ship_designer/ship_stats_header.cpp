#include "ship_stats_header.h"
#include <QBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <ui_ship_stats_header.h>

class ShipStatsHeaderPrivate {
 public:
  ShipStatsHeaderPrivate(ShipStatsHeader* _pub);
//  void setupUI();
//  QLayout* setupUIClass();
//  QLayout* setupUIHullType();
//  QLayout* setupUIShipSize();
//  QLayout* setupUICostMaint();
//  void translateUI();

 public:
  ShipStatsHeader* pub;
  Ui_ShipStatsHeader *ui;

//  QLabel* class_label;
//  QLineEdit* class_name;
//  QLabel* hull_type_label;
//  QPushButton* hull_type;
//  QLabel* ship_size_label;
//  QLabel* ship_size_amount;
//  QLabel* ship_cost_label;
//  QLabel* ship_maint_label;
//  QLabel* ship_cost_amount;
//  QLabel* ship_maint_amount;
};

ShipStatsHeaderPrivate::ShipStatsHeaderPrivate(ShipStatsHeader* _pub)
    : pub(_pub), ui(new Ui_ShipStatsHeader)
{
  ui->setupUi(pub);
  pub->setMinimumSize(200, 85);
  pub->setMaximumHeight(85);
  //  setupUI();
  //  translateUI();
}
//void ShipStatsHeaderPrivate::setupUI() {
//  QHBoxLayout* main_layout = new QHBoxLayout();
//  main_layout->setObjectName("ship_stats_header_layout");

//  QVBoxLayout* class_hull_size_layout = new QVBoxLayout();
//  class_hull_size_layout->addLayout(setupUIClass());

//  QHBoxLayout* hull_layout = new QHBoxLayout;
//  hull_layout->addLayout(setupUIHullType());
//  hull_layout->addLayout(setupUIShipSize());

//  class_hull_size_layout->addLayout(hull_layout);
//  main_layout->addLayout(class_hull_size_layout);
//  main_layout->addLayout(setupUICostMaint());
//  delete pub->layout();
//  pub->setLayout(main_layout);
//}
//QLayout* ShipStatsHeaderPrivate::setupUIClass() {
//  QHBoxLayout* class_layout = new QHBoxLayout;
//  class_layout->setObjectName("class_name_layout");
//  class_label = new QLabel;
//  class_label->setObjectName("class_label");
//  class_label->setMaximumSize(55, 25);
//  class_layout->addWidget(class_label);
//  class_name = new QLineEdit;
//  class_name->setObjectName("class_name");
//  class_name->setFrame(false);
//  class_name->setMaximumHeight(25);
//  class_layout->addWidget(class_name);
//  return class_layout;
//}

//QLayout* ShipStatsHeaderPrivate::setupUIHullType() {
//  QHBoxLayout* hull_type_layout = new QHBoxLayout;
//  hull_type_layout->setObjectName("hull_type_layout");
//  hull_type_label = new QLabel;
//  hull_type_label->setObjectName("hull_type_label");
//  hull_type_layout->addWidget(hull_type_label);
//  hull_type = new QPushButton;
//  hull_type->setObjectName("hull_type_button");
//  //  hull_type->setMargin
//  hull_type_layout->addWidget(hull_type);
//  return hull_type_layout;
//}
//QLayout* ShipStatsHeaderPrivate::setupUIShipSize() {
//  QHBoxLayout* ship_size_layout = new QHBoxLayout();
//  ship_size_layout->setObjectName("ship_size_layout");

//  ship_size_label = new QLabel;
//  ship_size_label->setObjectName("ship_size_label");
//  ship_size_layout->addWidget(ship_size_label);

//  ship_size_amount = new QLabel;
//  ship_size_amount->setObjectName("ship_size_amount");
//  ship_size_layout->addWidget(ship_size_amount);
//  return ship_size_layout;
//}
//QLayout* ShipStatsHeaderPrivate::setupUICostMaint() {
//  QGridLayout* cost_maint_layout = new QGridLayout;
//  cost_maint_layout->setObjectName("cost_maint_layout");

//  ship_cost_label = new QLabel;
//  ship_cost_label->setObjectName("ship_cost_label");
//  cost_maint_layout->addWidget(ship_cost_label, 0, 0);

//  ship_cost_amount = new QLabel;
//  ship_cost_amount->setObjectName("ship_cost_amount");
//  cost_maint_layout->addWidget(ship_cost_amount, 0, 1);

//  ship_maint_label = new QLabel;
//  ship_maint_label->setObjectName("ship_maint_label");
//  cost_maint_layout->addWidget(ship_maint_label, 1, 0);

//  ship_maint_amount = new QLabel;
//  ship_maint_amount->setObjectName("ship_maint_amount");
//  cost_maint_layout->addWidget(ship_maint_amount, 1, 1);

//  return cost_maint_layout;
//}

//void ShipStatsHeaderPrivate::translateUI() {
//  class_label->setText(QObject::tr("Class"));
//  ship_size_label->setText(QObject::tr("Ship size"));
//  hull_type_label->setText(QObject::tr("Hull type"));
//  ship_cost_label->setText(QObject::tr("Cost"));
//  ship_maint_label->setText(QObject::tr("Maint"));
//}

/////////////////////////////////////////////
ShipStatsHeader::ShipStatsHeader(QWidget* parent)
    : QWidget(parent), pimpl(new ShipStatsHeaderPrivate(this)) {
}

ShipStatsHeader::~ShipStatsHeader() {}
