#ifndef SHIP_STATS_HEADER_H
#define SHIP_STATS_HEADER_H

#include <QWidget>
#include <memory>

class ShipStatsHeaderPrivate;

class ShipStatsHeader : public QWidget
{
  Q_OBJECT
public:
  ShipStatsHeader(QWidget *parent = 0);
  ~ShipStatsHeader();

private:
  std::unique_ptr<ShipStatsHeaderPrivate> pimpl;
};

#endif // SHIP_STATS_HEADER_H
