#ifndef MAIN_WINDOW_HPP
#define MAIN_WINDOW_HPP
#include <data/data_thread.h>
#include <gui/battle_scene.h>
#include <gui/zoomable_graphics_view.h>
#include <logging_base.h>
#include <memory>
#include <QGraphicsScene>
//#include <QGraphicsSvgItem>
#include <QGraphicsView>
#include <QMainWindow>
//#include <QSvgRenderer>
#include <QTableView>

#include <data/fleets_orders_model.h>
#include <gui/ship_designer/ship_stats_header.h>

class MainWindow : public QMainWindow, public LoggingBase {
  Q_OBJECT

public:
  MainWindow(DataThread::ptr dataThread_);
  ~MainWindow();

private:
  void setupUI();
  void createFleetView();

private:
  QTableView *fleetView;
  DataThread::ptr dataThread;
  zoomable_graphic_view *view;
  battle_scene scene;
  Fleets::FleetsOrdersModel fleetModel;
};
#endif  // MAIN_WINDOW_HPP
