#pragma once
#include <QGraphicsScene>
#include <memory>
#include <boost/property_tree/ptree.hpp>

class battle_scene_private;

class battle_scene : public QGraphicsScene
{
public:
  battle_scene(boost::property_tree::ptree settings_tree);
  ~battle_scene();
  void populate();
//  void setup(GraphicsDistrRouterConfig);
private:
  friend class battle_scene_private;
  std::unique_ptr<battle_scene_private> pimpl;
};
