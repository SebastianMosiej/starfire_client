#include <QAction>
#include <QHeaderView>
#include <QMenu>
#include <data/fleets_orders_columns.hpp>
#include <gui/fleet_orders_context_menu.hpp>

namespace Fleets
{
FleetsOrdersView::FleetsOrdersView(QWidget *parent) : QTableView(parent)
{
  horizontalHeader()->setStretchLastSection(true);
  setContextMenuPolicy(Qt::CustomContextMenu);
  bool bResult = QObject::connect(this, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(customMenuRequested(QPoint)));
  logConnection("FleetsOrdersView", "customMenuRequest", bResult);
}

QMenu * FleetsOrdersView::generateFlyToSubmenu()
{
  QMenu *menu = new QMenu;
  menu->addAction(tr("Laterus"));
  return menu;
}

QMenu *FleetsOrdersView::generateContextMenu(QModelIndex idx)
{
  QMenu *menu = nullptr;
  if (idx.column() >= fc_FirstOrder)
  {
    menu = new QMenu(this);
    menu->addAction(tr("Patrol"));
    menu->addSeparator();
    menu->setObjectName("FleetOrdersContextMenu");
    QAction * flyTo = new QAction(tr("Fly to ..."));
    menu->addAction(flyTo);
    flyTo->setMenu(generateFlyToSubmenu());
    menu->addAction(tr("Build ..."));
    menu->addAction(tr("Assembly ..."));
  }
  return menu;
}

void FleetsOrdersView::customMenuRequested(QPoint pos)
{
  QModelIndex idx = indexAt(pos);
  if (!idx.isValid())
  {
    int a = 1;
  }
  QMenu *menu = generateContextMenu(idx);
  if (menu) menu->exec(mapToGlobal(pos));
  // check if where (cells) click was performed
}
}
