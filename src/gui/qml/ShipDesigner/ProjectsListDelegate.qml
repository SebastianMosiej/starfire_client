import QtQuick 2.0
import QtQuick.Layouts 1.3

Component {
    id: nameDelegate
    RowLayout{
        Rectangle {
            anchors.fill: parent
        }

//        anchors.fill: parent
        Text {
            text: name;
            wrapMode: Text.WordWrap
            Layout.maximumHeight: 25
            Layout.preferredWidth: 95
        }
        Text {
            text: size;
            Layout.preferredWidth: 30
            Layout.maximumWidth: 40
            Layout.maximumHeight: 25
        }
        Text {
            text: cost;
            Layout.maximumWidth: 45
            Layout.maximumHeight: 25
        }
    }
}
