import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3


ListView {
    id: projectsList
    Layout.fillWidth: true
    Layout.fillHeight: true
    Layout.minimumHeight: 100
    Layout.minimumWidth: 100

    model: ProjectsModel{}
    delegate: ProjectsListDelegate {}
}
