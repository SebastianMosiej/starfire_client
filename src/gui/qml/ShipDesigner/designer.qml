import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3

Item {
    height: 600
    width: 600
    Layout.preferredHeight: 600
    Layout.preferredWidth: 600
    Layout.maximumWidth: 600
    Layout.maximumHeight: 600
    Rectangle {
        id: designerBackground
        color: white
        anchors.fill: parent
    }
    RowLayout {
        anchors.rightMargin: 0
        anchors.bottomMargin: -2
        anchors.leftMargin: 0
        anchors.topMargin: 2
        Layout.fillWidth: true
        anchors.fill:parent
        ColumnLayout {
            Layout.fillWidth: true
            Layout.fillHeight: true
            //            Layout.fillHeight: true
            Layout.maximumWidth: parent.width*7/10
            Layout.minimumWidth: parent.width*7/10 - parent.spacing/2
            ShipHeader {
                Layout.fillWidth: true
                Layout.fillHeight: false
            }
            ShipComponents {
                Layout.fillWidth: true
                Layout.fillHeight: true
            }
        }
        ProjectsList {
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.preferredWidth: parent.width*3/10 - parent.spacing/2
            Layout.maximumWidth: parent.width*3/10
        }
    }
}
