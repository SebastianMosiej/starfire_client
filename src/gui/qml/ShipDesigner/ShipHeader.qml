import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3


Rectangle {
    width: 450
    height: 130
    color: white
    ColumnLayout {
        id: columnLayout
        anchors.fill: parent

        RowLayout {
            Text {
                id: projectNameLabel
                Layout.preferredWidth: 80
                text: "Project name"
                textFormat: Text.PlainText
            }
            TextField {
                id: projectName
                Layout.fillWidth: true
            }
        }
        GridLayout {
            Text {
                Layout.row: 0
                Layout.column: 0
                text: "Hull type"
            }
            Text {
                Layout.row: 1
                Layout.column: 0
                text: "Generation"
            }
            Text {
                Layout.row: 2
                Layout.column: 0
                text: "Size"
            }
            Button {
                Layout.row: 0
                Layout.column: 1
                text: "ES"
                Layout.fillWidth: true
                Layout.maximumWidth: 35
            }
            Text {
                Layout.row: 1
                Layout.column: 1
                text: "-"
                Layout.maximumWidth: 35
                Layout.fillWidth: true
                horizontalAlignment: Text.AlignHCenter
            }
            Text {
                Layout.row: 2
                Layout.column: 1
                text: "15"
                Layout.maximumWidth: 35
                Layout.fillWidth: true
                horizontalAlignment: Text.AlignHCenter
            }
            Item {
                id: horzSpread2
                Layout.row:0
                Layout.column: 2
                Layout.fillWidth: true
            }
            Text {
                Layout.row: 0
                Layout.column: 3
                text: "Speed"
            }
            Text {
                Layout.row: 1
                Layout.column: 3
                text: "Eng size"
            }
            Text {
                Layout.row: 2
                Layout.column: 3
                text: "Turn mode"
            }
            Text {
                Layout.row: 0
                Layout.column: 4
                text: "4"
                Layout.preferredWidth: 35
                horizontalAlignment: Text.AlignHCenter
            }
            Text {
                Layout.row: 1
                Layout.column: 4
                text: "0.33"
                Layout.preferredWidth: 35
                horizontalAlignment: Text.AlignHCenter
            }
            Text {
                Layout.row: 2
                Layout.column: 4
                Layout.preferredWidth: 35
                text: "2"
                horizontalAlignment: Text.AlignHCenter
            }
            Item {
                id: horzSpread3
                Layout.row:0
                Layout.column: 5
                Layout.fillWidth: true
            }
            Text {
                Layout.row: 0
                Layout.column: 6
                text: "HS Cost"
            }
            Text {
                Layout.row: 1
                Layout.column: 6
                text: "Cost"
            }
            Text {
                Layout.row: 2
                Layout.column: 6
                text: "Maint"
            }
            Text {
                Layout.row: 0
                Layout.column: 7
                text: "0.5"
                Layout.preferredWidth: 35
                horizontalAlignment: Text.AlignHCenter
            }
            Text {
                Layout.row: 1
                Layout.column: 7
                text: "354"
                Layout.preferredWidth: 35
                horizontalAlignment: Text.AlignHCenter
            }
            Text {
                Layout.row: 2
                Layout.column: 7
                Layout.preferredWidth: 35
                text: "75"
                horizontalAlignment: Text.AlignHCenter
            }
        }
        RowLayout {
            Item {
                id: horzSpread1
                Layout.fillWidth: true
            }
            CheckBox {
                id: acFast
                text: qsTr("Fast")
            }
            CheckBox {
                id: acRapid
                text: qsTr("Rapid")
            }
            CheckBox {
                id: acCheckbox
                text: qsTr("AC")
            }
            Text {
                id: htkLabel
                text: qsTr("HTK")
                Layout.minimumWidth: 40
                font.pixelSize: 12
            }
            Text {
                id: htkAmount
                text: qsTr("2")
                font.pixelSize: 12
            }
        }
    }
}
