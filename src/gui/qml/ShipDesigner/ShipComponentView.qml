import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3

Item {
    Layout.fillHeight: true
    Layout.fillWidth: true
    width: 400
    height: 300
    RowLayout {
        anchors.fill: parent
        TableView {
            Layout.fillHeight: true
            Layout.fillWidth: true
            TableViewColumn {
                role: "category"
                title: "Category"
                width: 100
                movable: false
                horizontalAlignment: Text.AlignHCenter
            }
            TableViewColumn {
                role: "type"
                title: "Type"
                width: 50
                movable: false
                horizontalAlignment: Text.AlignHCenter
            }
            TableViewColumn {
                role: "gen"
                title: "Gener"
                width: 50
                movable: false
                horizontalAlignment: Text.AlignHCenter
            }
            TableViewColumn {
                role: "cost"
                title: "Cost"
                width: 50
                movable: false
                horizontalAlignment: Text.AlignHCenter
            }
            TableViewColumn {
                role: "space"
                title: "Space"
                width: 50
                movable: false
                horizontalAlignment: Text.AlignHCenter
            }
            TableViewColumn {
                role: "count"
                title: "#"
                width: 50
                movable: false
                horizontalAlignment: Text.AlignHCenter
            }
        }
    }
}
