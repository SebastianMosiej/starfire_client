import QtQuick 2.9
import QtQuick.Controls 2.2

ListModel {
    id: projectsModel1
    ListElement { name: "LRPt mk2"; size: "60"; cost: "748" }
    ListElement { name: "LRPt mk3"; size: "60"; cost: "869" }
    ListElement { name: "WP Assault EastGate"; size: "42"; cost: "338" }
    ListElement { name: "SR Escort mk3"; size: "16"; cost: "182" }
}
