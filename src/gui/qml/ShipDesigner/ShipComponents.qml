import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3


Item {
    Layout.preferredWidth: 400
    Layout.preferredHeight: 300
//    width: 400
//    height: 300
    TabView {
        anchors.fill: parent
        frameVisible: false
        Tab {
            title: "Weapons"
            ShipComponentView {}
        }
        Tab {
            title: "Defense"
            ShipComponentView {}
        }
        Tab {
            title: "Main"
            ShipComponentView {}
        }
        Tab {
            title: "Other"
            ShipComponentView {}
        }
    }
}
