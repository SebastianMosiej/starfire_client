#include <gui/battle_scene.h>
#include <data/generate_hex_path.h>
#include <gui/svg/graphics_hex_path.h>
#include <QtWidgets/QGraphicsPathItem>
#include <cmath>

class battle_scene_private {
 public:
  battle_scene_private(battle_scene* _pub,
                       boost::property_tree::ptree settings_tree);

 public:
  battle_scene* pub;
  boost::property_tree::ptree settings;
};

battle_scene_private::battle_scene_private(
    battle_scene* _pub, boost::property_tree::ptree settings_tree)
    : pub(_pub), settings(settings_tree) {}
////////////////////////////////////////////////////
battle_scene::battle_scene(boost::property_tree::ptree settings_tree)
    : pimpl(new battle_scene_private(this, settings_tree)) {}
battle_scene::~battle_scene() {}

void battle_scene::populate() {
  unsigned int side_length(pimpl->settings.get<unsigned int>(
      "starfire_client.gui.graphics.hex_side_length"));
  unsigned int width = (unsigned int)ceil(side_length * sqrt(3) / 2);
  GenerateHexPath gen;
  gen.SideLength(side_length);
  QPainterPath path = gen.generate();
  QRectF rect;
  for (unsigned int y = 0; y < 30; y++) {
    for (unsigned int i = 0; i < 30; i++) {
      QGraphicsPathItem* hex = new QGraphicsPathItem();
      hex->setPath(path);
      if (!rect.isValid()) rect = hex->boundingRect();
      hex->setX(width * 2 * i);
      if (y % 2 == 1) hex->setX(hex->x() + width);
      hex->setY(side_length * 1.5 * y);
      addItem(hex);
    }
  }
}
