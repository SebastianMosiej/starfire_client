#include <QMouseEvent>
#include <QWheelEvent>
#include <boost/format.hpp>
#include <gui/zoomable_graphics_view.h>

void zoomable_graphic_view::wheelEvent(QWheelEvent* event)
{
    setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
    // Scale the view / do the zoom
    double scaleFactor = 1.15;
    if (event->delta() > 0) {
        // Zoom in
        scale(scaleFactor, scaleFactor);
    } else {
        // Zooming out
        scale(1.0 / scaleFactor, 1.0 / scaleFactor);
    }
}

void zoomable_graphic_view::mousePressEvent(QMouseEvent* event)
{
    if (event->button() == Qt::MiddleButton) {
        // Store original position.
        mouse_origin_pos = event->pos();
    }
}

void zoomable_graphic_view::mouseMoveEvent(QMouseEvent* event)
{
    if (event->buttons() & Qt::MiddleButton) {
        QPointF oldp        = mapToScene(mouse_origin_pos);
        QPointF newp        = mapToScene(event->pos());
        QPointF translation = newp - oldp;

        translate(translation.x()/3, translation.y()/3);

        mouse_origin_pos = event->pos();
    }
}
