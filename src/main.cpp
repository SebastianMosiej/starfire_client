#include <QtWidgets/QApplication>
#include <QtCore/QThread>
#include <memory>
#include <loggers.h>
#include <log4cplus/initializer.h>
#include <gui/main_window.h>
#include <data/data_thread.h>

int main(int argc, char* argv[])
{
  log4cplus::Initializer initializer;
  QApplication app(argc, argv);
  app.setApplicationName("StarFire Client");
  app.setOrganizationName("Khlavan");
  create_loggers();

  QThread* thread = new QThread;
  DataThread::ptr data_thread(new DataThread());
  data_thread->moveToThread(thread);
  thread->start();

  std::unique_ptr<MainWindow> main_window(new MainWindow(data_thread));
  main_window->show();

  //////////////////////////////////////////////////////////////////////////
  int iReturn = app.exec();
  main_window.release();
  thread->quit();
  return iReturn;
}
