#ifndef TESTRUNNER_H
#define TESTRUNNER_H

// std includes
#include <algorithm>
#include <functional>
#include <iostream>
#include <list>
#include <iterator>

//////////////////////////////////////////////////////////////////////////
// Test Runner allows automatic execution of tests
class TestRunner {
  using testRunnerFun = std::function<int(int,char*[])>;
 public:
  static TestRunner& Instance() {
    static TestRunner instance;
    return instance;
  }

  char RegisterTestClassRunner(testRunnerFun fun)
  {
    m_testClassRunner.push_back(fun);
    return char(1);
  }

  int RunAllTestRunners(int argc, char* argv[]) {
    int errorCode = 0;
    std::for_each(begin(m_testClassRunner), end(m_testClassRunner),
                  [&](testRunnerFun & fun) {
                    errorCode |= fun(argc, argv);
                    std::cout << std::endl;
                  });

    return errorCode;
  }

 private:
  std::list<testRunnerFun> m_testClassRunner;
};

#define DECLARE_TEST_RUNNER(className)  \
  namespace { \
    int executeTestClass(int argc, char* argv[]) \
    { \
      className test; \
      return QTest::qExec(&test, argc, argv); \
    }; \
    static char test_##className = \
        TestRunner::Instance().RegisterTestClassRunner(executeTestClass); \
  }

// Use this macro to execute all tests
#define RUN_ALL_QTESTS_RUNNERS(argc, argv) TestRunner::Instance().RunAllTestRunners(argc, argv)

#endif  // TESTRUNNER_H
