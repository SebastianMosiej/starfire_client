#include <TestRunner.h>
#include <log4cplus/initializer.h>
#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>
#include <tools/loggers.h>

int main(int argc, char *argv[])
{
  int          result = 0;
  QApplication app(argc, argv);

  try
  {
    log4cplus::Initializer initializer;
    create_loggers();
    result = RUN_ALL_QTESTS_RUNNERS(argc, argv);
  }
  catch (std::exception &e)
  {
    LOG4CPLUS_FATAL(log4cplus::Logger::getRoot(), "main()- Exception occured: " << e.what());
    result = -1;
  }
  catch (...)
  {
    LOG4CPLUS_FATAL(log4cplus::Logger::getRoot(), "main()- Exception occured");
    result = -1;
  }
  return result;
}
