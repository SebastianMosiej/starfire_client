#include <QtTest/QtTest>
#include <TestRunner.h>
#include "ut_generate_hex_path.h"

DECLARE_TEST_RUNNER(GenerateHexPathTest)

void GenerateHexPathTest::init() {
  test_object.reset(new GenerateHexPath());
}
void GenerateHexPathTest::cleanup() { test_object.reset(); }

void GenerateHexPathTest::test_set_side_length()
{
  test_object->SideLength(15);
  QCOMPARE(test_object->SideLength(),15u);
}

void GenerateHexPathTest::test_generate()
{
  test_object->SideLength(15);
  QPainterPath path =  test_object->generate();
  QVERIFY(!path.isEmpty());
  QCOMPARE(path.elementCount(),7);
}
