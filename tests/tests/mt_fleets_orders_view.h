#include <memory>
#include <gui/fleet_orders_context_menu.hpp>
#include <data/fleets_orders_model.h>

class FleetsOrdersViewFriend : public Fleets::FleetsOrdersView
{
public:
    QMenu * generateContextMenuFriend(QModelIndex idx) { return generateContextMenu(idx); }
};

class FleetsOrdersViewTest : public QObject
{
    Q_OBJECT
public:
    std::shared_ptr<Fleets::FleetsOrdersView> test_object;
    std::shared_ptr<Fleets::FleetsOrdersModel> test_model;
    //FleetDetails get_fleet_date(int orders_count);

private Q_SLOTS:
    void init();
    void cleanup();

    void test_context_menu_on_order_field();
};
