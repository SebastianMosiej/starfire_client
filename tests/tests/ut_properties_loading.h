#include <memory>
#include <data/data_thread.h>

class PropertiesLoadingTest : public QObject
{
  Q_OBJECT
public:
  std::shared_ptr<DataThread> test_object;

private Q_SLOTS:
  void init();
  void cleanup();
  void test_properties_file_found_and_loaded();
  void test_get_hex_side_length();
  void test_columns_width();
};
