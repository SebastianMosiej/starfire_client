#include <TestRunner.h>
#include <tests/ut_properties_loading.h>
#include <QtTest/QtTest>

DECLARE_TEST_RUNNER(PropertiesLoadingTest)

void PropertiesLoadingTest::init() { test_object.reset(new DataThread); }
void PropertiesLoadingTest::cleanup() { test_object.reset(); }
void PropertiesLoadingTest::test_properties_file_found_and_loaded() {
  boost::property_tree::ptree settings = test_object->GetSettings();
  QVERIFY(settings.size() > 0);
}
void PropertiesLoadingTest::test_get_hex_side_length() {
  boost::property_tree::ptree settings = test_object->GetSettings();
  unsigned int side_length(0);
  try {
    side_length = settings.get<unsigned int>(
        "starfire_client.gui.graphics.hex_side_length");
  } catch (boost::property_tree::ptree_bad_path& /* ex */) {
    QVERIFY(false);
  }
  QCOMPARE(side_length, 24u);
}

void PropertiesLoadingTest::test_columns_width() {
  boost::property_tree::ptree settings = test_object->GetSettings();
  try {
    int column_width(0);
    column_width = settings.get<int>(
        "starfire_client.gui.views.fleet_orders.columns_width.name");
    QCOMPARE(column_width, 150);
    column_width = settings.get<int>(
        "starfire_client.gui.views.fleet_orders.columns_width.speed");
    QCOMPARE(column_width, 42);
    column_width = settings.get<int>(
        "starfire_client.gui.views.fleet_orders.columns_width.orders");
    QCOMPARE(column_width, 120);
    column_width = settings.get<int>(
        "starfire_client.gui.views.fleet_orders.columns_width.leader");
    QCOMPARE(column_width, 100);
    column_width = settings.get<int>(
        "starfire_client.gui.views.fleet_orders.columns_width.status");
    QCOMPARE(column_width, 100);
    column_width = settings.get<int>(
        "starfire_client.gui.views.fleet_orders.columns_width.position");
    QCOMPARE(column_width, 100);
  } catch (boost::property_tree::ptree_bad_path& /* ex */) {
    QVERIFY(false);
  }
}
