#include <memory>
#include <data/fleets_orders_model.h>

class FleetsOrdersModelTest : public QObject
{
    Q_OBJECT
public:
    std::shared_ptr<Fleets::FleetsOrdersModel> test_object;

private Q_SLOTS:
    void init();
    void cleanup();
    void test_header_data_empty();
    void test_header_data_columns();
    void test_header_columns_width();

    void test_orders_column_color_1();
    void test_orders_column_color_2();
    void test_orders_column_count_1();
    void test_orders_column_count_2();
    void test_columns_width_1();

    void test_settings();
};
