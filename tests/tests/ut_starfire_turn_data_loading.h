#include <memory>
#include <data/data_thread.h>

class StarFireTurnDataLoadingTest : public QObject
{
  Q_OBJECT
public:
  std::shared_ptr<DataThread> test_object;

public Q_SLOTS:

  void init();
  void cleanup();

};
