#include <QtTest/QtTest>
#include <TestRunner.h>
#include <data/turn_data/fleet_detail.hpp>
#include <data/fleets_orders_columns.hpp>
#include <tests/mt_fleets_orders_view.h>
#include <QMenu>
#include <sstream>

DECLARE_TEST_RUNNER(FleetsOrdersViewTest)

void FleetsOrdersViewTest::init()
{
  test_object.reset(new Fleets::FleetsOrdersView());
  test_model.reset(new Fleets::FleetsOrdersModel());
}
void FleetsOrdersViewTest::cleanup()
{
  test_object.reset();
  test_model.reset();
}

void FleetsOrdersViewTest::test_context_menu_on_order_field()
{
  // GIVEN
  FleetsOrdersViewFriend *  view = new FleetsOrdersViewFriend();
  test_object->setModel(test_model.get());
  test_object->show();

  FleetOrders fleet;
  fleet.name = "1.Fleet";
  fleet.currentPosition = "Laterus";
  fleet.leaderName = "Flower";
  fleet.speed = 5;
  fleet.status = "Patrol";
  fleet.orders.push_back("Fly to Laterus");
  fleet.orders.push_back("Patrol");
  test_model->addFleetDetails(fleet);

  // WHEN
  QModelIndex idx = test_model->index(0, Fleets::fc_FirstOrder);
  QMenu *     menu = view->generateContextMenuFriend(idx);
  // THEN
  QVERIFY(menu != nullptr);
  QList<QAction *> list = menu->actions();
  QVERIFY(list.size() > 0);
  QAction *act = list.at(0);
  QCOMPARE(act->text(), QString("Patrol"));
  act = list.at(2);
  QCOMPARE(act->text(), QString("Fly to ..."));
  QMenu *submenu = act->menu();
  QVERIFY(submenu != nullptr);
}
