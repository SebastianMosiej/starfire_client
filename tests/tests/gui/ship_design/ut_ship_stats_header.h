#include <memory>
#include <gui/ship_designer/ship_stats_header.h>

class ShipStatsHeaderTest : public QObject
{
  Q_OBJECT
public:
  std::shared_ptr<ShipStatsHeader> test_object;

private Q_SLOTS:
  void init();
  void cleanup();
  void test_widget_size();
  void test_project_name_presence();
  void test_project_name_sizes();
  void test_project_name_position();
  void test_hull_type();
  void test_ship_size();
  void test_ship_cost_maint();
  void test_label_text();
};
