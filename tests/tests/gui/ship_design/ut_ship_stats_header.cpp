#include <QtTest/QtTest>
#include <TestRunner.h>
#include "ut_ship_stats_header.h"
#include <QBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QSizePolicy>

DECLARE_TEST_RUNNER(ShipStatsHeaderTest)

void ShipStatsHeaderTest::init() {
  test_object.reset(new ShipStatsHeader);
  test_object->show();
}

void ShipStatsHeaderTest::cleanup() { test_object.reset(); }
void ShipStatsHeaderTest::test_widget_size() {
  QVERIFY2(test_object->width() >= 200,
           qPrintable(QString("widget width (%1) should be at least 200")
                          .arg(test_object->width())));
  QVERIFY2(test_object->height() >= 85,
           qPrintable(QString("widget heigth (%1) should be at least 85")
                          .arg(test_object->height())));
}
void ShipStatsHeaderTest::test_project_name_presence() {
  QLabel* label = test_object->findChild<QLabel*>("project_name_label");
  QLineEdit* edit = test_object->findChild<QLineEdit*>("project_name_edit");
  QVERIFY(edit != nullptr);
  QVERIFY(label != nullptr);
  QHBoxLayout* parent_layout =
      test_object->findChild<QHBoxLayout*>("project_name_layout");
  QVERIFY(parent_layout != nullptr);
  QCOMPARE(label->parent(), edit->parent());
  QCOMPARE(label->parent(), test_object.get());
}
void ShipStatsHeaderTest::test_project_name_sizes() {
  QLabel* label = test_object->findChild<QLabel*>("project_name_label");
  QLineEdit* edit = test_object->findChild<QLineEdit*>("project_name_edit");
  QVERIFY(label != nullptr);
  QVERIFY(edit != nullptr);
  QCOMPARE(label->maximumHeight(), 25);
  QCOMPARE(label->maximumWidth(), 100);
  QCOMPARE(edit->maximumHeight(), 25);
  QVERIFY(!edit->hasFrame());
  QCOMPARE(edit->maximumHeight(), 25);
}
void ShipStatsHeaderTest::test_project_name_position() {
  QLabel* label = test_object->findChild<QLabel*>("project_name_label");
  QLineEdit* edit = test_object->findChild<QLineEdit*>("project_name_edit");
  QVERIFY(label != nullptr);
  QVERIFY(edit != nullptr);
  QHBoxLayout* layout =
      test_object->findChild<QHBoxLayout*>("project_name_layout");
  QVERIFY(layout != nullptr);
  QVERIFY2(
      layout->indexOf(label) >= 0,
      qPrintable(
          QString("%1 is not present in layout").arg(label->objectName())));
  QVERIFY2(
      layout->indexOf(edit) >= 0,
      qPrintable(
          QString("%1 is not present in layout").arg(label->objectName())));
  QVERIFY2(label->x() < edit->x(),
           qPrintable(QString("Horizontal position of label (%1) should be "
                              "smaller then edit (%2)")
                          .arg(label->x())
                          .arg(edit->x())));
}
void ShipStatsHeaderTest::test_hull_type() {
  QLabel* hull_label = test_object->findChild<QLabel*>("hull_type_label");
  QVERIFY(hull_label != nullptr);
  QPushButton* hull_button =
      test_object->findChild<QPushButton*>("hull_type_button");
  QVERIFY(hull_button != nullptr);
  QGridLayout* parent_layout =
      test_object->findChild<QGridLayout*>("gridLayout");
  QVERIFY(parent_layout != nullptr);
  QCOMPARE(hull_label->parent(), hull_button->parent());
  QCOMPARE(hull_label->parent(), test_object.get());
  QCOMPARE(hull_label->height() / 2 + hull_label->y(),
           hull_button->height() / 2 + hull_button->y());
}
void ShipStatsHeaderTest::test_ship_size() {
  QLabel* size_label = test_object->findChild<QLabel*>("size_label");
  QVERIFY2(size_label != nullptr, qPrintable(QString("ship_size_label not found")));
  QLabel* size_amount_label = test_object->findChild<QLabel*>("size_amount");
  QVERIFY2(size_amount_label != nullptr,
           qPrintable(QString("ship_size_amount not found")));
  QCOMPARE(size_label->parent(), size_amount_label->parent());
}
void ShipStatsHeaderTest::test_ship_cost_maint() {
//  QLabel* ship_cost_label = test_object->findChild<QLabel*>("ship_cost_label");
//  QVERIFY2(ship_cost_label != nullptr,
//           qPrintable(QString("ship_cost_label not found")));
//  QLabel* ship_maint_label =
//      test_object->findChild<QLabel*>("ship_maint_label");
//  QVERIFY2(ship_maint_label != nullptr,
//           qPrintable(QString("ship_maint_label not found")));
//  QLabel* ship_cost_amount =
//      test_object->findChild<QLabel*>("ship_cost_amount");
//  QVERIFY2(ship_cost_amount != nullptr,
//           qPrintable("ship_cost_amount not found"));
//  QLabel* ship_maint_amount =
//      test_object->findChild<QLabel*>("ship_maint_amount");
//  QVERIFY2(ship_maint_amount != nullptr,
//           qPrintable("ship_maint_amount not found"));
//  QCOMPARE(ship_cost_label->parent(), ship_maint_label->parent());
}
void ShipStatsHeaderTest::test_label_text() {
//  QLabel* class_label = test_object->findChild<QLabel*>("class_label");
//  QVERIFY2(class_label != nullptr,
//           qPrintable("ship_maint_amount not found"));
//  QCOMPARE(class_label->text(), tr("Class"));
//  QLabel* ship_size_label = test_object->findChild<QLabel*>("ship_size_label");
//  QCOMPARE(ship_size_label->text(), tr("Ship size"));
//  QLabel* hull_type_label = test_object->findChild<QLabel*>("hull_type_label");
//  QCOMPARE(hull_type_label->text(), tr("Hull type"));
//  QLabel* ship_cost_label = test_object->findChild<QLabel*>("ship_cost_label");
//  QCOMPARE(ship_cost_label->text(), tr("Cost"));
//  QLabel* ship_maint_label =
//      test_object->findChild<QLabel*>("ship_maint_label");
//  QCOMPARE(ship_maint_label->text(), tr("Maint"));
}
