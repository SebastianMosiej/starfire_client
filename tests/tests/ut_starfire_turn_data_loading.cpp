#include <TestRunner.h>
#include <QtTest>
#include <memory>
#include <data/data_thread.h>
#include <tests/ut_starfire_turn_data_loading.h>

DECLARE_TEST_RUNNER(StarFireTurnDataLoadingTest)

void StarFireTurnDataLoadingTest::init()
{ test_object.reset(new DataThread); }
void StarFireTurnDataLoadingTest::cleanup() { test_object.reset(); }
