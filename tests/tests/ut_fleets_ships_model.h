#include <memory>
#include <data/fleets_ships_model.h>

class FleetsShipsModelTest : public QObject
{
    Q_OBJECT
public:
    std::shared_ptr<Fleets::FleetsShipsModel> test_object;
    Ships::ShipDetails createRandomShip();

private Q_SLOTS:
    void init();
    void cleanup();
    void test_invalid_index();
    void test_index();
    void test_column_count_fleet_invalid();
    void test_column_count_fleet();
    void test_column_count_ships();
    void test_row_count_initial();
    void test_row_count_added();
    void test_first_row();
    void test_data_fleet_1();
};
