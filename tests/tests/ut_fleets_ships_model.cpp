#include <QtTest/QtTest>
#include <TestRunner.h>
#include "ut_fleets_ships_model.h"
#include <random>

DECLARE_TEST_RUNNER(FleetsShipsModelTest)

Ships::ShipDetails FleetsShipsModelTest::createRandomShip()
{
  Ships::ShipDetails ship;
  ship.className = "Long Range Torpedo ";
  std::random_device rd;
  std::mt19937 random(rd());
  std::uniform_int_distribution<int> dist(1, 100);
  ship.className += std::to_string(dist(random));
  
  return ship;
}
void FleetsShipsModelTest::init() { test_object.reset(new Fleets::FleetsShipsModel()); }
void FleetsShipsModelTest::cleanup() { test_object.reset(); }
void FleetsShipsModelTest::test_row_count_initial() { QCOMPARE(test_object->rowCount(QModelIndex()), 0); }
void FleetsShipsModelTest::test_invalid_index()
{
  QModelIndex idx = test_object->index(-1, -1);
  QVERIFY(!idx.isValid());
  idx = test_object->index(0, 0);
  QVERIFY(!idx.isValid());
}

void FleetsShipsModelTest::test_row_count_added()
{
  // GIVEN
  Ships::ShipDetails ship(createRandomShip());
  // WHEN
  test_object->addShip(ship);
  // THEN
  QCOMPARE(test_object->rowCount(QModelIndex()), 1);
}
void FleetsShipsModelTest::test_column_count_fleet_invalid() { QCOMPARE(test_object->columnCount(), 0); }
void FleetsShipsModelTest::test_column_count_fleet()
{
  // GIVEN
  Ships::ShipDetails ship(createRandomShip());
  // WHEN
  test_object->addShip(ship);
  // THEN
  QCOMPARE(test_object->columnCount(), 1);
}

void FleetsShipsModelTest::test_column_count_ships()
{
  // GIVEN
  Ships::ShipDetails ship(createRandomShip());
  // WHEN
  test_object->addShip(ship);
  QModelIndex idx = test_object->index(0, 0);
  // THEN
  QCOMPARE(test_object->columnCount(idx), 9);
}

void FleetsShipsModelTest::test_index()
{
  // GIVEN
  Ships::ShipDetails ship(createRandomShip());
  // WHEN
  test_object->addShip(ship);
  // THEN
  QModelIndex idx = test_object->index(0, 0);
  QVERIFY(idx.isValid());
  QModelIndex shipIdx = test_object->index(0, 0, idx);
  QVERIFY(shipIdx.isValid());
}

void FleetsShipsModelTest::test_first_row() { }
void FleetsShipsModelTest::test_data_fleet_1()
{
  // GIVEN
  Ships::ShipDetails ship(createRandomShip());
  // WHEN
  test_object->addShip(ship);
  QModelIndex idx = test_object->index(0, 0);
  // THEN
  QCOMPARE(idx.data(Qt::DisplayPropertyRole).toString(), QString(ship.className.c_str()) + " Fleet");
}
