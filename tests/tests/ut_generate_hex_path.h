#include <memory>
#include <data/generate_hex_path.h>

class GenerateHexPathTest : public QObject
{
    Q_OBJECT
public:
    std::shared_ptr<GenerateHexPath> test_object;

private Q_SLOTS:
    void init();
    void cleanup();
    void test_set_side_length();
    void test_generate();
};
