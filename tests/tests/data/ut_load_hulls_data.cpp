#include <QtTest/QtTest>
#include <TestRunner.h>
#include "tests/data/ut_load_hulls_data.h"
#include <QFile>

DECLARE_TEST_RUNNER(StarFireXmlLoaderTest)

namespace
{
  const std::string path("starfire_data.xml");
}

void StarFireXmlLoaderTest::init()
{
  test_object.reset(new Data::StarFireXmlLoader());
}
void StarFireXmlLoaderTest::cleanup()
{
  test_object.reset();
}

void StarFireXmlLoaderTest::test_load_empty_path()
{
  QVERIFY(!test_object->load(""));
}

void StarFireXmlLoaderTest::test_load_incorrect_path()
{
  QVERIFY(!test_object->load("incorect_file.xml"));
}

void StarFireXmlLoaderTest::test_hulls_count()
{
  //GIVEN
  QVERIFY(QFile::exists(path.c_str()));
  //WHEN
  QVERIFY(test_object->load(path));
  //THEN
  std::vector<Data::HullData> hullsData = test_object->getHullData();
  QCOMPARE(hullsData.size(),20ul);
}

void StarFireXmlLoaderTest::test_verify_hull_name()
{
  QVERIFY(test_object->load(path));
  std::vector<Data::HullData> hullsData = test_object->getHullData();
  QVERIFY(hullsData.size()>1ul);
  Data::HullData hull = hullsData[0];
  QCOMPARE(hull.name.c_str(), "EX");
  hull = hullsData[6];
  QCOMPARE(hull.name.c_str(), "CA");
  hull = hullsData[10];
  QCOMPARE(hull.name.c_str(), "SD");
  hull = hullsData[16];
  QCOMPARE(hull.name.c_str(), "MM");

}
