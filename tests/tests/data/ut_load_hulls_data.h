#include <QObject>
#include <memory>
#include <data/parser/starfire_xml_load.hpp>
#include <data/parser/starfire_xml_load_p.hpp>

class StarFireXmlLoaderTest : public QObject
{
    Q_OBJECT
public:
    std::shared_ptr<Data::StarFireXmlLoader> test_object;
    std::shared_ptr<Data::StarFireXmlLoaderPrivate> test_object_2;

private Q_SLOTS:
    void init();
    void cleanup();
    void test_load_empty_path();
    void test_load_incorrect_path();
    void test_hulls_count();
    void test_verify_hull_name();
};
