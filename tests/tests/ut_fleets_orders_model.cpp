#include <QtTest/QtTest>
#include <TestRunner.h>
#include "ut_fleets_orders_model.h"
#include <data/turn_data/fleet_detail.hpp>
#include <QtWidgets/QTableView>
#include <QContextMenuEvent>
#include <QMainWindow>
#include <QMenu>
#include <QtWidgets/QTableView>
#include <data/fleets_orders_columns.hpp>
#include <gui/fleet_orders_context_menu.hpp>
#include <sstream>

DECLARE_TEST_RUNNER(FleetsOrdersModelTest);

namespace
{
FleetOrders generateFleetOrders()
{
  FleetOrders fleet;
  fleet.name = "1.Fleet";
  fleet.currentPosition = "Laerus";
  fleet.leaderName = "Flower";
  fleet.speed = 5;
  fleet.status = "Patrol";
  fleet.orders.push_back("Fly to Laterus");
  fleet.orders.push_back("Patrol");
  return fleet;
}
FleetOrders get_fleet_date(int orders_count)
{
  std::stringstream ss;
  FleetOrders       fleet;
  fleet.name = "1.Fleet";
  for (int i = 0; i < orders_count; i++)
  {
    ss.str("order ");
    ss << i;
    fleet.orders.push_back(ss.str());
  }

  return fleet;
}
}

void FleetsOrdersModelTest::init() { test_object.reset(new Fleets::FleetsOrdersModel()); }
void FleetsOrdersModelTest::cleanup() { test_object.reset(); }
void FleetsOrdersModelTest::test_header_data_empty()
{
  QCOMPARE(test_object->headerData(0, Qt::Vertical), QVariant());
  QCOMPARE(test_object->headerData(1, Qt::Vertical), QVariant());
}

void FleetsOrdersModelTest::test_header_data_columns()
{
  QCOMPARE(test_object->headerData(0, Qt::Horizontal), QVariant("Name"));
  QCOMPARE(test_object->headerData(1, Qt::Horizontal), QVariant("Leader"));
  QCOMPARE(test_object->headerData(2, Qt::Horizontal), QVariant("Status"));
  QCOMPARE(test_object->headerData(3, Qt::Horizontal), QVariant("Speed"));
  QCOMPARE(test_object->headerData(4, Qt::Horizontal), QVariant("Position"));
  QCOMPARE(test_object->headerData(5, Qt::Horizontal), QVariant("StMP 1"));
  QCOMPARE(test_object->headerData(6, Qt::Horizontal), QVariant("StMP 2"));
  QCOMPARE(test_object->headerData(7, Qt::Horizontal), QVariant("StMP 3"));
  QCOMPARE(test_object->headerData(8, Qt::Horizontal), QVariant("StMP 4"));
  QCOMPARE(test_object->headerData(9, Qt::Horizontal), QVariant("StMP 5"));
}

void FleetsOrdersModelTest::test_header_columns_width()
{
  // GIVEN
  QTableView* view = new QTableView();
  view->setModel(test_object.get());
  view->show();

  test_object->addFleetDetails(get_fleet_date(4));

  // WHEN
  view->resizeColumnsToContents();
  // THEN
  QCOMPARE(view->columnWidth(Fleets::fc_Name), 151);
  QCOMPARE(view->columnWidth(Fleets::fc_Leader), 101);
  delete view;
}

void FleetsOrdersModelTest::test_orders_column_count_1()
{
  // GIVEN/WHEN
  test_object->addFleetDetails(get_fleet_date(4));
  // THEN
  QCOMPARE(test_object->columnCount(QModelIndex()), 10);
}

void FleetsOrdersModelTest::test_orders_column_count_2()
{
  // GIVEN
  // WHEN
  test_object->addFleetDetails(get_fleet_date(4));
  test_object->addFleetDetails(get_fleet_date(6));
  // THEN
  QCOMPARE(test_object->columnCount(QModelIndex()), 12);
}

void FleetsOrdersModelTest::test_orders_column_color_1()
{
  // GIVEN
  int orders_count = 3;
  // WHEN
  test_object->addFleetDetails(get_fleet_date(orders_count));
  // THEN
  QCOMPARE(test_object->columnCount(), orders_count + 6);
  QCOMPARE(test_object->index(0, 5).data(Qt::BackgroundColorRole), QVariant());
  QCOMPARE(test_object->index(0, 6).data(Qt::BackgroundColorRole), QVariant());
  QCOMPARE(test_object->index(0, 7).data(Qt::BackgroundColorRole), QVariant());
  QCOMPARE(test_object->index(0, 8).data(Qt::BackgroundColorRole), QVariant(QColor(Qt::gray)));
}

void FleetsOrdersModelTest::test_orders_column_color_2()
{
  // GIVEN/WHEN
  test_object->addFleetDetails(get_fleet_date(3));
  int orders_count = 5;
  test_object->addFleetDetails(get_fleet_date(orders_count));
  QCOMPARE(test_object->columnCount(), orders_count + 6);
  // THEN
  QCOMPARE(test_object->index(0, 5).data(Qt::BackgroundColorRole), QVariant());
  QCOMPARE(test_object->index(0, 6).data(Qt::BackgroundColorRole), QVariant());
  QCOMPARE(test_object->index(0, 7).data(Qt::BackgroundColorRole), QVariant());
  QCOMPARE(test_object->index(0, 8).data(Qt::BackgroundColorRole), QVariant(QColor(Qt::gray)));
  QCOMPARE(test_object->index(0, 9).data(Qt::BackgroundColorRole), QVariant(QColor(Qt::gray)));
  QCOMPARE(test_object->index(0, 10).data(Qt::BackgroundColorRole), QVariant(QColor(Qt::gray)));

  QCOMPARE(test_object->index(1, 8).data(Qt::BackgroundColorRole), QVariant());
  QCOMPARE(test_object->index(1, 9).data(Qt::BackgroundColorRole), QVariant());
  QCOMPARE(test_object->index(1, 10).data(Qt::BackgroundColorRole), QVariant(QColor(Qt::gray)));
}

void FleetsOrdersModelTest::test_columns_width_1()
{
  // GIVEN
  QTableView* view = new QTableView();
  view->setModel(test_object.get());
  view->show();

  FleetOrders fleet = generateFleetOrders();
  test_object->addFleetDetails(fleet);
  // WHEN
  view->resizeColumnsToContents();
  // THEN
  QCOMPARE(view->columnWidth(Fleets::fc_Name), 151);
  QCOMPARE(view->columnWidth(Fleets::fc_Leader), 101);
  QCOMPARE(view->columnWidth(Fleets::fc_Status), 101);
  QCOMPARE(view->columnWidth(Fleets::fc_Speed), 43);
  QCOMPARE(view->columnWidth(Fleets::fc_Position), 101);
  QCOMPARE(view->columnWidth(Fleets::fc_FirstOrder), 121);
  QCOMPARE(view->columnWidth(Fleets::fc_FirstOrder + 1), 121);
  QCOMPARE(view->columnWidth(Fleets::fc_FirstOrder + 2), 121);
  QCOMPARE(view->columnWidth(Fleets::fc_FirstOrder + 3), 0);
  delete view;
}

void FleetsOrdersModelTest::test_settings()
{
  // GIVEN
  int nameColumnWidth(80);
  int leaderColumnWidth(70);
  int statusColumnWidth(60);
  int speedColumnWidth(90);
  int positionColumnWidth(72);
  int ordersColumnWidth(78);
  boost::property_tree::ptree properties;
  properties.add("columns_width.name", nameColumnWidth);
  properties.add("columns_width.leader", leaderColumnWidth);
  properties.add("columns_width.status", statusColumnWidth);
  properties.add("columns_width.speed", speedColumnWidth);
  properties.add("columns_width.position", positionColumnWidth);
  properties.add("columns_width.orders", ordersColumnWidth);
  test_object->setProperties(properties);

  QTableView* view = new QTableView();
  view->setModel(test_object.get());
  view->show();

  FleetOrders fleet = generateFleetOrders();
  test_object->addFleetDetails(fleet);
  // WHEN
  view->resizeColumnsToContents();
  // THEN
  QCOMPARE(view->columnWidth(Fleets::fc_Name), nameColumnWidth+1);
  QCOMPARE(view->columnWidth(Fleets::fc_Leader), leaderColumnWidth+1);
  QCOMPARE(view->columnWidth(Fleets::fc_Status), statusColumnWidth+1);
  QCOMPARE(view->columnWidth(Fleets::fc_Speed), speedColumnWidth+1);
  QCOMPARE(view->columnWidth(Fleets::fc_Position), positionColumnWidth+1);
  QCOMPARE(view->columnWidth(Fleets::fc_FirstOrder), ordersColumnWidth+1);

  delete view;
}
