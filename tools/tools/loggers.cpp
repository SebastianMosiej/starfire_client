#include <log4cplus/configurator.h>
#include <loggers.h>
#include <QDir>
#include <QFileInfo>
#include <boost/format.hpp>
#include <iostream>
using namespace log4cplus;

namespace
{
  namespace Const
  {
    const QString cLogDir("log");
    const QString cDebugLoggerName("debug");
    const QString cSlotsLoggerName("slots");
    const QString cGuiLoggerName("gui");
    const QString cDebugLoggerFile = QString("%1%2%3").arg(cLogDir).arg(QDir::separator()).arg(cDebugLoggerName);
    const QString cSlotsLoggerFile = QString("%1%2%3").arg(cLogDir).arg(QDir::separator()).arg(cSlotsLoggerName);
    const QString cGuiLoggerFile   = QString("%1%2%3").arg(cLogDir).arg(QDir::separator()).arg(cGuiLoggerName);

    const QString cLoggerPropertiesFileName("/log4cplus.properties");
    const QString cLoggerPropertiesFilePath =
        QString("%1%2%3").arg(cLogDir).arg(QDir::separator()).arg(cLoggerPropertiesFileName);
  }

  bool bLoggersCreated(false);

  void deleteFileIfExist(const QString& filePath)
  {
    QFile file(filePath);
    if (file.exists()) file.remove();
  }
  void cleanupLogsDir()
  {
    // be sure that DIR is created
    QDir directory;
    directory.mkdir(Const::cLogDir);
    // cleanup logs dir content
    deleteFileIfExist(Const::cDebugLoggerFile);
    deleteFileIfExist(Const::cGuiLoggerFile);
    deleteFileIfExist(Const::cSlotsLoggerFile);
  }

  QString get_logger_name(eLoggerType loggerType)
  {
    switch (loggerType)
    {
      case eDebug:
        return Const::cDebugLoggerFile;
      case eSlots:
        return Const::cSlotsLoggerFile;
      case eGUI:
        return Const::cGuiLoggerFile;
    }
    return QString();
  }
}

void
create_loggers()
{
  cleanupLogsDir();
  if (!QFileInfo::exists(Const::cLoggerPropertiesFilePath)) {
    std::cout << "Not existing log4cpp properties file" << std::endl;
  }
  log4cplus::PropertyConfigurator::doConfigure(Const::cLoggerPropertiesFilePath.toStdString());
  bLoggersCreated = true;
  //
  printLog(eDebug, eInfoLogLevel, "Log created");
  printLog(eGUI, eInfoLogLevel, "Log created");
  printLog(eSlots, eInfoLogLevel, "Log created");
}

void
printLog(eLoggerType loggerType, eLogLevel debugLevel, const QString& strMsg)
{
  if (false == bLoggersCreated) create_loggers();

  QString logger_name = get_logger_name(loggerType);
  if (logger_name.isEmpty()) return;
  Logger logger = log4cplus::Logger::getInstance(logger_name.toStdString());
  log4cplus::LogLevel logLevel = 0;
  switch(debugLevel)
  {
    case eInfoLogLevel:
      logLevel = log4cplus::INFO_LOG_LEVEL;
      break;
    case eWarningLogLevel:
      logLevel = log4cplus::WARN_LOG_LEVEL;
      break;
    case eDebugLogLevel:
      logLevel = log4cplus::DEBUG_LOG_LEVEL;
      break;
    case eErrorLogLevel:
      logLevel = log4cplus::FATAL_LOG_LEVEL;
      break;
    default:
      return;
  }
  logger.log(logLevel,strMsg.toStdString());

}

//void
//printLog(eLoggerType loggerType, eLogLevel debugLevel, const std::string& strMsg)
//{
//  if (false == bLoggersCreated) create_loggers();

//  boost::optional<std::string> logger_name = get_logger_name(loggerType);
//  if (!logger_name) return;
//}

//////////////////////////////////////////////////////////////////////////
