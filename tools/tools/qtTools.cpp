#include <qtTools.h>
#include <loggers.h>

void logConnection(const QString & strClassName,const QString &strConnDesc,bool bResult)
{
  QString strDebug = QString("%1: Connection %2").arg(strClassName).arg(strConnDesc);
	if (bResult)
	{
		strDebug+=" SUCCESS";	
		printLog(eSlots, eDebugLogLevel, strDebug);
	}
	else
	{
		strDebug+=" --FAIL--";
		printLog(eSlots, eWarningLogLevel, strDebug);
	}
  Q_ASSERT_X(bResult==true,strClassName.toLatin1(),strDebug.toLatin1());
}
