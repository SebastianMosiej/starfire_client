#ifndef LOGGERS_INCLUDE_
#define LOGGERS_INCLUDE_
#include <string>
#include <QString>

enum eLogLevel
{
  eInfoLogLevel = 0,
  eWarningLogLevel,
  eDebugLogLevel,
  eErrorLogLevel
};

enum eLoggerType
{
  eDebug = 0,
  eGUI,
  eSlots
};

void create_loggers();
void destroyLoggers();
//void printLog(eLoggerType loggerType, eLogLevel debugLevel, const std::string &strMsg);
void printLog(eLoggerType loggerType, eLogLevel debugLevel, const QString &strMsg);
#endif  // LOGGERS_INCLUDE_
