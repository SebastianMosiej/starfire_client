#include <TestRunner.h>
#include <loggers.h>
#include <QApplication>

int main(int argc, char *argv[]) 
{ 
  QApplication app(argc, argv);
  create_loggers();
  return RUN_ALL_QTESTS_RUNNERS(argc, argv);
}
