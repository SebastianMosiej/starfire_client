#include <memory>
#include <game_turn_to_property_tree_exporter.h>
#include <QObject>

class GameTurnToPropertyTreeExporterTest : public QObject
{
    Q_OBJECT
public:
    std::shared_ptr<GameTurnToPropertyTreeExporter> test_object;

private:
  Ships::ShipDetails generateShipDetails();
  FleetOrders generateFleetOrders(unsigned int orders_amount);
  void SavePropertyTreeToFile(const boost::property_tree::ptree &tree);

private Q_SLOTS:
    void init();
    void cleanup();

    void test_property_tree_for_root_namespace_tags();
    void test_file_with_0_ship_designs();
//    void test_ship_designs_validation_xsd();
    void test_1_ships_design();
    void test_2_ships_design();
    void test_file_with_0_fleets_orders();
    void test_file_with_1_fleets_orders();
    void test_file_with_2_fleets_orders();
    void test_0_fleets_details();
    void test_1_fleets_details();
    void test_2_fleets_details();
};
