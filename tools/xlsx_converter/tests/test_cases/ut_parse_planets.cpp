#include <test_cases/ut_parse_planets.h>
#include <TestRunner.h>

namespace {
    const std::string xlsx_file("turn_starfire.xlsx");
}
DECLARE_TEST_RUNNER(PlanetParserTest)

void PlanetParserTest::init() {
    test_object.reset(new SFXlsxParser(xlsx_file));
}

void PlanetParserTest::cleanup() { test_object.reset(); }


void PlanetParserTest::test_planet_count()
{
//    GIVEN/WHEN
    std::vector<Planet::Planets> planets = test_object->get_planets();
//    THEN
    QCOMPARE(planets.size(), 10ul);
}

void PlanetParserTest::test_planet_name()
{
//    GIVEN/WHEN
    std::vector<Planet::Planets> planets = test_object->get_planets();
//    THEN
    Planet::Planets planet = planets[0];
    QCOMPARE(planet.systemName, std::string("Rigel"));
}

void PlanetParserTest::test_planet_planet_number()
{
//    GIVEN/WHEN
    std::vector<Planet::Planets> planets = test_object->get_planets();
//    THEN
    Planet::Planets planet = planets[0];
    QCOMPARE(planet.planetNumber, 3u);
    QCOMPARE(planet.moonNumber, -1);
    QCOMPARE(planet.starNumber, -1);
}

void PlanetParserTest::test_planet_planet_moon()
{
//    GIVEN/WHEN
    std::vector<Planet::Planets> planets = test_object->get_planets();
//    THEN
    Planet::Planets planet = planets[3];
    QCOMPARE(planet.planetNumber, 3u);
    QCOMPARE(planet.moonNumber, 1);
    QCOMPARE(planet.starNumber, -1);
}

void PlanetParserTest::test_planet_star_planet_moon()
{
//    GIVEN/WHEN
    std::vector<Planet::Planets> planets = test_object->get_planets();
//    THEN
    Planet::Planets planet = planets[9];
    QCOMPARE(planet.planetNumber, 2u);
    QCOMPARE(planet.moonNumber, -1);
    QCOMPARE(planet.starNumber, 2);
}

void PlanetParserTest::test_planet_habitability()
{
//    GIVEN/WHEN
    std::vector<Planet::Planets> planets = test_object->get_planets();
//    THEN
  Planet::Planets planet = planets[0];
  QCOMPARE(planet.habitability, +Planet::WorldType::Bening);
  planet = planets[1];
  QCOMPARE(planet.habitability, +Planet::WorldType::Desolate);
  planet = planets[3];
  QCOMPARE(planet.habitability, +Planet::WorldType::Desolate);
}

void PlanetParserTest::test_planet_bodytype()
{
//    GIVEN/WHEN
  std::vector<Planet::Planets> planets = test_object->get_planets();
//    THEN
  Planet::Planets planet = planets[0];
  QCOMPARE(planet.bodyType, +Planet::BodyType::T);
}

void PlanetParserTest::test_planet_population()
{
//    GIVEN/WHEN
  std::vector<Planet::Planets> planets = test_object->get_planets();
//    THEN
  Planet::Planets planet = planets[0];
  QCOMPARE(planet.population, 2543.1);
  planet = planets[1];
  QCOMPARE(planet.population, 148.6);
  planet = planets[3];
  QCOMPARE(planet.population, 24.4);
  planet = planets[4];
  QCOMPARE(planet.population, 19.5);
  planet = planets[7];
  QCOMPARE(planet.population, 201.8);
}

void PlanetParserTest::test_planet_growth()
{
//    GIVEN/WHEN
  std::vector<Planet::Planets> planets = test_object->get_planets();
//    THEN
  Planet::Planets planet = planets[0];
  QCOMPARE(planet.growth, 30.5);
  planet = planets[1];
  QCOMPARE(planet.growth, 1.7);
  planet = planets[7];
  QCOMPARE(planet.growth, 2.4);
}

void PlanetParserTest::test_planet_maximumPopulation()
{
//    GIVEN/WHEN
  std::vector<Planet::Planets> planets = test_object->get_planets();
//    THEN
  Planet::Planets planet = planets[0];
  QCOMPARE(planet.maximumPopulation, 3750);
  planet = planets[1];
  QCOMPARE(planet.maximumPopulation, 180);
  planet = planets[3];
  QCOMPARE(planet.maximumPopulation, 60);
  planet = planets[7];
  QCOMPARE(planet.maximumPopulation, 3750);
}

void PlanetParserTest::test_planet_industrial_units()
{
//    GIVEN/WHEN
  std::vector<Planet::Planets> planets = test_object->get_planets();
//    THEN
  Planet::Planets planet = planets[0];
  QCOMPARE(planet.industrialUnit, 0u);
  planet = planets[2];
  QCOMPARE(planet.industrialUnit, 2u);
  planet = planets[4];
  QCOMPARE(planet.industrialUnit, 4u);
}

void PlanetParserTest::test_planet_rei()
{
//    GIVEN/WHEN
  std::vector<Planet::Planets> planets = test_object->get_planets();
//    THEN
  Planet::Planets planet = planets[0];
  QCOMPARE(planet.resourceExploitationIndex, 120u);
  planet = planets[7];
  QCOMPARE(planet.resourceExploitationIndex, 140u);
  planet = planets[8];
  QCOMPARE(planet.resourceExploitationIndex, 100u);
}

void PlanetParserTest::test_planet_income()
{
//    GIVEN/WHEN
  std::vector<Planet::Planets> planets = test_object->get_planets();
//    THEN
  Planet::Planets planet = planets[0];
  QCOMPARE(planet.income, 3684.95);
  planet = planets[7];
  QCOMPARE(planet.income, 341.14);
  planet = planets[8];
  QCOMPARE(planet.income, 224.28);
}

void PlanetParserTest::test_planet_buildings()
{
//    GIVEN/WHEN
  std::vector<Planet::Planets> planets = test_object->get_planets();
//    THEN
  Planet::Planets planet = planets[0];
  QCOMPARE(planet.buildings[Planet::BuildingsType::ScienceAcademy], 3u);
  QCOMPARE(planet.buildings[Planet::BuildingsType::ResearchDevelopmentStation], 6u);
  QCOMPARE(planet.buildings[Planet::BuildingsType::PilotTrainingFacility], 1u);
  QCOMPARE(planet.buildings[Planet::BuildingsType::QtTrainingFacility], 2u);
  QCOMPARE(planet.buildings[Planet::BuildingsType::PlanetaryScienceFacility], 4u);
  QCOMPARE(planet.buildings[Planet::BuildingsType::TradeHub], 5u);
  QCOMPARE(planet.buildings[Planet::BuildingsType::ImperialCommandCenter], 9u);
  QCOMPARE(planet.buildings[Planet::BuildingsType::ImperialCapital], 1u);
}
