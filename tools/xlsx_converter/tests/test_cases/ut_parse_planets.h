#include <memory>
#include <QObject>
#include <sf_xlsx_parser.h>

class PlanetParserTest : public QObject
{
    Q_OBJECT
public:
    std::shared_ptr<SFXlsxParser> test_object;

private Q_SLOTS:
    void init();
    void cleanup();

    void test_planet_count();
    void test_planet_name();
    void test_planet_planet_number();
    void test_planet_planet_moon();
    void test_planet_star_planet_moon();
    void test_planet_habitability();
    void test_planet_bodytype();
    void test_planet_population();
    void test_planet_growth();
    void test_planet_maximumPopulation();
    void test_planet_industrial_units();
    void test_planet_rei();
    void test_planet_income();
    void test_planet_buildings();
};
