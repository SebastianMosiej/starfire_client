#pragma once
#include <QObject>
#include <memory>
#include <game_turn_to_property_tree_exporter.h>

class TurnXmlFileTest : public QObject  {
  Q_OBJECT
public:
  std::unique_ptr<GameTurnToPropertyTreeExporter> exporter;

private Q_SLOTS:
  void init();
  void cleanup();
  void test_ship_designs_validation_xsd();
};

