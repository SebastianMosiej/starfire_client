#include <QtTest/QtTest>
#include <TestRunner.h>
#include "ut_parse_fleets_xlsx.h"
#include <data/turn_data/fleet_detail.hpp>

namespace {
  const std::string xlsx_file("turn_starfire.xlsx");
}
DECLARE_TEST_RUNNER(FleetsParserTest)

void FleetsParserTest::init() {
  test_object.reset(new SFXlsxParser(xlsx_file));
}

void FleetsParserTest::cleanup() { test_object.reset(); }

void FleetsParserTest::test_fleets_orders() {
  std::vector<FleetOrders> orders = test_object->get_fleet_orders();
  QCOMPARE(orders.size(),11ul);

  FleetOrders order = orders[0];
  QCOMPARE(order.name, std::string("1. Fleet"));
  QCOMPARE(order.leaderName, std::string(""));
  QCOMPARE(order.status, std::string("Patrol"));
  QCOMPARE(order.speed, 2u);
  QCOMPARE(order.currentPosition, std::string("Manticore"));
  QCOMPARE(order.orders.size(), static_cast<size_t>(1));

  order = orders[1];
  QCOMPARE(order.name, std::string("2. Fleet"));
  QCOMPARE(order.leaderName, std::string(""));
  QCOMPARE(order.status, std::string("---"));
  QCOMPARE(order.speed, 2u);
  QCOMPARE(order.currentPosition, std::string("Rigel/3"));
  QCOMPARE(order.orders.size(), static_cast<size_t>(1));
  QCOMPARE(order.orders[0],std::string("Patrol"));

  order = orders[7];
  QCOMPARE(order.name, std::string("First GeoSurvey"));
  QCOMPARE(order.leaderName, std::string(""));
  QCOMPARE(order.status, std::string("---"));
  QCOMPARE(order.speed, 4u);
  QCOMPARE(order.currentPosition, std::string("Kontar"));
  QCOMPARE(order.orders.size(),static_cast<size_t>(4));
  QCOMPARE(order.orders[0],std::string("Geo Survey Feros/1"));
  QCOMPARE(order.orders[1],std::string("Geo Survey Feros/1"));
  QCOMPARE(order.orders[2],std::string("Geo Survey Feros/1"));
  QCOMPARE(order.orders[3],std::string("Geo Survey Feros/1"));
}

void FleetsParserTest::test_fleets_ships() {
  std::vector<Fleet> fleets = test_object->get_fleets_composition();
  QCOMPARE(fleets.size(),11ul);

  Fleet fleet = fleets[0];
  QCOMPARE(fleet.name, std::string("1. Fleet"));
  QCOMPARE(fleet.ships.size(), 2ul);
  Ships::ShipDetails details = fleet.ships[0];
  QCOMPARE(details.className, std::string("LRPt"));
  QCOMPARE(details.amount, 4u);
  QCOMPARE(details.crewGrade.size(), 1ul);
  QVERIFY(details.crewGrade.find(Ships::bg_Crack)!=details.crewGrade.end());
  QCOMPARE(details.crewGrade[Ships::bg_Crack],4u);
  QCOMPARE(details.speedTactical, 5u);
  QCOMPARE(details.speedStategic, 2u);
  QCOMPARE(details.levelOfDetection, 1u);
  QCOMPARE(details.levelCloak, 0u);
  QCOMPARE(details.mothball, false);

  details = fleet.ships[1];
  QCOMPARE(details.className, std::string("SRE"));
  QCOMPARE(details.amount, 3u);
  QCOMPARE(details.crewGrade.size(), 1ul);
  QVERIFY(details.crewGrade.find(Ships::bg_Crack)!=details.crewGrade.end());
  QCOMPARE(details.crewGrade[Ships::bg_Crack],3u);
  QCOMPARE(details.speedTactical, 7u);
  QCOMPARE(details.speedStategic, 3u);
  QCOMPARE(details.levelOfDetection, 1u);
  QCOMPARE(details.levelCloak, 0u);
  QCOMPARE(details.mothball, false);

}

void FleetsParserTest::test_ships_projects() {
  std::vector<Ships::ShipProject> projects = test_object->get_ship_projects();
  QCOMPARE(projects.size(),17ul);
  Ships::ShipProject project = projects[0];

  QCOMPARE(project.className, std::string("LRPt"));
  QCOMPARE(project.hullType, std::string("DD"));
  QCOMPARE(project.hullSize, 30u);
  QCOMPARE(project.cost, 354.f);
  QCOMPARE(project.maintenance, 62.6f);
  QCOMPARE(project.eqList, std::string("Ptax3, (Ia)"));
  QCOMPARE(project.hit_to_kill, 15u);
  QCOMPARE(project.datalink, std::string("-"));
  QCOMPARE(project.multitarget, 1u);
  QCOMPARE(project.surveyPoints, 0u);
  QCOMPARE(project.sensorType, std::string("Ya"));
  QCOMPARE(project.cargoCapacity, 0u);
  QCOMPARE(project.peopleCapacity, 0u);
  QCOMPARE(project.magazineCapacity, 0u);
  QCOMPARE(project.transitModifier, 30u);
  QCOMPARE(project.turnMode, 3u);
  QCOMPARE(project.strategicSpeed, 5u);
  QCOMPARE(project.tacticalSpeed, 2u);
  QCOMPARE(project.design, std::string("SAAAYaPt(I)PtQaQa(I)(I)(I)(I)Pt"));

  project = projects[8];

  QCOMPARE(project.className, std::string("Cargo Freighter"));
  QCOMPARE(project.hullType, std::string("FT4"));
  QCOMPARE(project.hullSize, 30u);
  QCOMPARE(project.cost, 230.f);
  QCOMPARE(project.maintenance, 17.3f);
  QCOMPARE(project.eqList, std::string("H, (Ica)"));
  QCOMPARE(project.hit_to_kill, 14u);
  QCOMPARE(project.datalink, std::string("-"));
  QCOMPARE(project.multitarget, 1u);
  QCOMPARE(project.surveyPoints, 0u);
  QCOMPARE(project.sensorType, std::string("-"));
  QCOMPARE(project.cargoCapacity, 21u);
  QCOMPARE(project.peopleCapacity, 0u);
  QCOMPARE(project.magazineCapacity, 0u);
  QCOMPARE(project.transitModifier, 21u);
  QCOMPARE(project.turnMode, 2u);
  QCOMPARE(project.strategicSpeed, 4u);
  QCOMPARE(project.tacticalSpeed, 4u);
  QCOMPARE(project.design, std::string("Hx5(Ic)Hx6(Ic)Hx5(Ic)Hx5Q(Ic)"));

}
