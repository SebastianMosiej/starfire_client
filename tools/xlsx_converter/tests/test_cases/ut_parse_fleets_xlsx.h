#include <memory>
#include <sf_xlsx_parser.h>

class FleetsParserTest : public QObject
{
    Q_OBJECT
public:
    std::shared_ptr<SFXlsxParser> test_object;

private Q_SLOTS:
    void init();
    void cleanup();
    void test_fleets_orders();
    void test_fleets_ships();
    void test_ships_projects();
};
