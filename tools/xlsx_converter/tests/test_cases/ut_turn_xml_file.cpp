#include <test_cases/ut_turn_xml_file.h>
#include <QtXmlPatterns/QAbstractMessageHandler>
#include <QtXmlPatterns/QXmlSchema>
#include <QtXmlPatterns/QXmlSchemaValidator>
#include <QtCore/QFile>
#include <QtTest/qtestcase.h>
#include <boost/filesystem/path.hpp>

namespace
{
  namespace fs = boost::filesystem;
  const fs::path kExportDirectory("game_data");
  const fs::path kExportFileName("starfire_turn.xml");
  const fs::path kExportFilePath = kExportDirectory / kExportFileName;
  const std::string kRootTag("StarFire_Turn");
  const std::string kRootXmlnsTag("<xmlattr>.xmlns");
  const std::string kXmlnsAttribVal("https://www.w3schools.com");
  const std::string kRootXmlnsXsiTag("<xmlattr>.xmlns:xsi");
  const std::string kXmlnsXsiAttribVal("http://www.w3.org/2001/XMLSchema-instance");
  const std::string kRootSchemeTag("<xmlattr>.xsi:schemaLocation");
  const std::string kSchemeLocationVal("https://www.w3schools.com turn.xsd");

  class MessageHandler : public QAbstractMessageHandler
  {
  public:
    MessageHandler()
        : QAbstractMessageHandler(0)
    {
    }

    QString statusMessage() const
    {
      return m_description;
    }

    int line() const
    {
      return m_sourceLocation.line();
    }

    int column() const
    {
      return m_sourceLocation.column();
    }

  protected:
    virtual void handleMessage(QtMsgType type, const QString &description, const QUrl &identifier,
                               const QSourceLocation &sourceLocation)
    {
      Q_UNUSED(type);
      Q_UNUSED(identifier);

      m_description    = description;
      m_sourceLocation = sourceLocation;
    }

  private:
    QString         m_description;
    QSourceLocation m_sourceLocation;
  };
}
void TurnXmlFileTest::init()
{
  exporter.reset(new GameTurnToPropertyTreeExporter);
}

void TurnXmlFileTest::cleanup()
{
  exporter.reset();
}

void
TurnXmlFileTest::test_ship_designs_validation_xsd()
{
  // GIVEN
  SFGameData data;
  // WHEN
  exporter->ExportToPropertyTree(data);
  // THEN
  // Validata result file with XSD
  QXmlSchema     schema;
  MessageHandler msgHandler;
  QFile          schemaFile("turn.xsd");
  QVERIFY(schemaFile.open(QIODevice::ReadOnly));
  schema.setMessageHandler(&msgHandler);
  QVERIFY2(schema.load(&schemaFile, QUrl::fromLocalFile(schemaFile.fileName())), "turn.xsd file is not valid");

  if (!schema.isValid()) {
    QVERIFY2(false, "Scheme file turn.xsd is not valid");
  }
  else
  {
    QXmlSchemaValidator validator(schema);
    QFile               xmlFile(kExportFilePath.string().c_str());
    xmlFile.open(QIODevice::ReadOnly);
    if (!validator.validate(&xmlFile)) {
      std::string msg("XML file turn.xml is not valid - (");
      msg += msgHandler.statusMessage().toStdString();
      QVERIFY2(false, msg.c_str());
    }
  }
}

#include "ut_turn_xml_file.h"
