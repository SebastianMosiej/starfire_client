#include <TestRunner.h>
#include <test_cases/ut_game_turn_to_property_tree_exporter_test.h>
#include <QtTest/QtTest>
#include <boost/property_tree/xml_parser.hpp>
//#include <boost/property_tree/ptree.hpp>
#include <boost/filesystem.hpp>
#include <fstream>
#include <iostream>
#include <random>

namespace
{
  int executeTestClass(int argc, char *argv[])
  {
    GameTurnToPropertyTreeExporterTest test;
    return QTest::qExec(&test, argc, argv);
  };
  static char test_XmlExporterTest = TestRunner::Instance().RegisterTestClassRunner(executeTestClass);
}

namespace
{
  namespace fs = boost::filesystem;
  const fs::path kExportDirectory("game_data");
  const fs::path kExportFileName("starfire_turn.xml");
  const fs::path kExportFilePath = kExportDirectory / kExportFileName;
  const std::string kRootTag("StarFire_Turn");
  const std::string kRootXmlnsTag("<xmlattr>.xmlns");
  const std::string kXmlnsAttribVal("https://www.w3schools.com");
  const std::string kRootXmlnsXsiTag("<xmlattr>.xmlns:xsi");
  const std::string kXmlnsXsiAttribVal("http://www.w3.org/2001/XMLSchema-instance");
  const std::string kRootSchemeTag("<xmlattr>.xsi:schemaLocation");
  const std::string kSchemeLocationVal("https://www.w3schools.com turn.xsd");
}

using boost::property_tree::ptree;

void
GameTurnToPropertyTreeExporterTest::SavePropertyTreeToFile(const boost::property_tree::ptree &tree)
{
  try {
    boost::property_tree::xml_parser::xml_writer_settings<std::__cxx11::string> settings(' ', 2);
    write_xml(kExportFilePath.string(), tree, std::locale(), settings);
  } catch (boost::property_tree::xml_parser::xml_parser_error const & ex) {
    return;
  }
}

Ships::ShipDetails
GameTurnToPropertyTreeExporterTest::generateShipDetails()
{
  // Seed with a real random value, if available
  std::random_device r;
  // Choose a random mean between 1 and 6
  std::default_random_engine e1(r());

  Ships::ShipDetails ship;
  ship.className        = "SR Escort mk2";
  ship.amount           = 14;
  ship.crewGrade        = {{Ships::bg_Average, 13}, {Ships::bg_Crack, 1}};
  ship.speedTactical    = 6;
  ship.speedStategic    = 3;
  ship.levelOfDetection = 1;
  ship.levelCloak       = 0;
  ship.mothball         = false;
  ship.notes            = "";
  return ship;
}

FleetOrders
GameTurnToPropertyTreeExporterTest::generateFleetOrders(unsigned int orders_amount)
{
  std::vector<std::string> orders_examples = {
      "Move to Kontar", "Move to bedard",        "Move to Feros Star", "Assembly SRW Base", "Survey Rigel Conceal",
      "Survey Rigel",   "Survey Kontar Conceal", "Survey Kontar",      "Warp Survey",       ""};
  // Seed with a real random value, if available
  std::random_device r;
  // Choose a random mean between 1 and 6
  std::default_random_engine e1(r());

  FleetOrders orders;
  orders.leaderName      = "Leader";
  orders.speed           = orders_amount;
  orders.currentPosition = "Rigel";

  std::uniform_int_distribution<size_t> uniform_dist(0, orders_examples.size() - 1);

  for (unsigned int order = 0; order < orders_amount; order++) {
    size_t mean = uniform_dist(e1);
    orders.orders.push_back(orders_examples[mean]);
  }
  size_t mean = static_cast<size_t>(uniform_dist(e1));
  orders.name = std::to_string(mean) + ".Fleet";

  return orders;
}

void
GameTurnToPropertyTreeExporterTest::init()
{
  test_object.reset(new GameTurnToPropertyTreeExporter);
}

void
GameTurnToPropertyTreeExporterTest::cleanup()
{
  test_object.reset();
}

void
GameTurnToPropertyTreeExporterTest::test_property_tree_for_root_namespace_tags()
{
  // GIVEN
  SFGameData data;

  // WHEN
  boost::property_tree::ptree dataTree = test_object->ExportToPropertyTree(data);
  // THEN
  try
  {
    boost::property_tree::ptree &ship_designs = dataTree.get_child("StarFire_Turn.<xmlattr>.xmlns");
//    QVERIFY(!ship_desings.size());
  }
  catch (boost::property_tree::ptree_bad_path const &ex)
  {
    SavePropertyTreeToFile(dataTree);
    QVERIFY2(false, QString("Incorrect path: %1").arg(ex.what()).toLatin1());
    return;
  }
}


void
GameTurnToPropertyTreeExporterTest::test_file_with_0_ship_designs()
{
  // GIVEN
  SFGameData data;

  // WHEN
  boost::property_tree::ptree dataTree = test_object->ExportToPropertyTree(data);
  // THEN

  try
  {
    boost::property_tree::ptree ship_desings = dataTree.get_child("StarFire_Turn.projects");
    QVERIFY(!ship_desings.size());
  }
  catch (boost::property_tree::ptree_bad_path const &ex)
  {
    QVERIFY2(false, QString("Incorrect path: %1").arg(ex.what()).toLatin1());
    return;
  }
}

void
GameTurnToPropertyTreeExporterTest::test_1_ships_design()
{
  // GIVEN
  SFGameData         data;
  Ships::ShipProject project;
  project.className                    = "LRPt mk4";
  project.hullType                     = "CA";
  project.hullSize                     = 60;
  project.cost                         = 869;
  project.maintenance                  = 149.5;
  project.hit_to_kill                  = 34;
  project.datalink                     = "a";
  project.multitarget                  = 1;
  project.surveyPoints                 = 0;
  project.cargoCapacity                = 0;
  project.peopleCapacity               = 0;
  project.magazineCapacity             = 0;
  project.transitModifier              = 42;
  project.turnMode                     = 4;
  project.strategicSpeed               = 2;
  project.tacticalSpeed                = 5;
  project.design                       = "Sx4Ax10Mg(IbIb)Ptb(IbIb)QbPtb(IbIb)PtbQb(IbIb)PtbQbPtb(IbIb)Yca";
  data.shipProjects[project.className] = project;

  // WHEN
  boost::property_tree::ptree dataTree = test_object->ExportToPropertyTree(data);
  // THEN
  try
  {
    ptree ship_designs = dataTree.get_child("StarFire_Turn.projects");
    QVERIFY(ship_designs.size());
    ptree::iterator iter   = ship_designs.begin();
    ptree           design = iter->second;
    QCOMPARE(design.get<std::string>("name"), std::string("LRPt mk4"));
    QCOMPARE(design.get<std::string>("hull_type"), std::string("CA"));
    QCOMPARE(design.get<std::string>("hs_size"), std::string("60"));
    QCOMPARE(design.get<std::string>("cost"), std::string("869"));
    QCOMPARE(design.get<std::string>("maintenance"), std::string("149.5"));
    QCOMPARE(design.get<std::string>("HTK"), std::string("34"));
    QCOMPARE(design.get<std::string>("datalinks"), std::string("a"));
    QCOMPARE(design.get<std::string>("multitarget"), std::string("1"));
    QCOMPARE(design.get<std::string>("survey_pts"), std::string("0"));
    QCOMPARE(design.get<std::string>("scaner"), std::string(""));
    QCOMPARE(design.get<std::string>("hold"), std::string("0"));
    QCOMPARE(design.get<std::string>("people_capacity"), std::string("0"));
    QCOMPARE(design.get<std::string>("magazines"), std::string("0"));
    QCOMPARE(design.get<std::string>("transit_mod"), std::string("42"));
    QCOMPARE(design.get<std::string>("speed.turn_mode"), std::string("4"));
    QCOMPARE(design.get<std::string>("speed.cruise"), std::string("2"));
    QCOMPARE(design.get<std::string>("speed.battle"), std::string("5"));
    QCOMPARE(design.get<std::string>("components"),
             std::string("Sx4Ax10Mg(IbIb)Ptb(IbIb)QbPtb(IbIb)PtbQb(IbIb)PtbQbPtb(IbIb)Yca"));
  }
  catch (boost::property_tree::ptree_bad_path const &ex)
  {
    QVERIFY2(false, QString("Incorrect path: %1").arg(ex.what()).toLatin1());
    return;
  }
}

void
GameTurnToPropertyTreeExporterTest::test_2_ships_design()
{
  // GIVEN
  SFGameData         data;
  Ships::ShipProject project;
  project.className                    = "LRPt mk4";
  project.hullType                     = "CA";
  project.hullSize                     = 60;
  project.cost                         = 869;
  project.maintenance                  = 149.5;
  project.hit_to_kill                  = 34;
  project.datalink                     = "a";
  project.multitarget                  = 1;
  project.surveyPoints                 = 0;
  project.cargoCapacity                = 0;
  project.peopleCapacity               = 0;
  project.magazineCapacity             = 0;
  project.transitModifier              = 42;
  project.turnMode                     = 4;
  project.strategicSpeed               = 2;
  project.tacticalSpeed                = 5;
  project.design                       = "Sx4Ax10Mg(IbIb)Ptb(IbIb)QbPtb(IbIb)PtbQb(IbIb)PtbQbPtb(IbIb)Yca";
  data.shipProjects[project.className] = project;

  Ships::ShipProject project2;
  project2.className                    = "WarpSurvey mk2";
  project2.hullType                     = "EX";
  project2.hullSize                     = 6;
  project2.cost                         = 76;
  project2.maintenance                  = 13.40f;
  project2.hit_to_kill                  = 3;
  project2.datalink                     = "";
  project2.multitarget                  = 1;
  project2.surveyPoints                 = 5;
  project2.cargoCapacity                = 1;
  project2.peopleCapacity               = 0;
  project2.magazineCapacity             = 0;
  project2.transitModifier              = 5;
  project2.turnMode                     = 2;
  project2.strategicSpeed               = 4;
  project2.tacticalSpeed                = 2;
  project2.design                       = "HXeQa(Ic)";
  data.shipProjects[project2.className] = project2;

  // WHEN
  boost::property_tree::ptree dataTree = test_object->ExportToPropertyTree(data);
  // THEN

  try
  {
    ptree ship_desings = dataTree.get_child("StarFire_Turn.projects");
    QCOMPARE(ship_desings.size(), 2ul);
    ptree::iterator iter = ship_desings.begin();
    iter++;
    ptree design = iter->second;
    QCOMPARE(design.get<std::string>("name"), std::string("WarpSurvey mk2"));
    QCOMPARE(design.get<std::string>("hull_type"), std::string("EX"));
    QCOMPARE(design.get<std::string>("hs_size"), std::string("6"));
    QCOMPARE(design.get<std::string>("cost"), std::string("76"));
    QCOMPARE(design.get<float>("maintenance"), 13.4f);
    QCOMPARE(design.get<std::string>("HTK"), std::string("3"));
    QCOMPARE(design.get<std::string>("datalinks"), std::string(""));
    QCOMPARE(design.get<std::string>("multitarget"), std::string("1"));
    QCOMPARE(design.get<std::string>("survey_pts"), std::string("5"));
    QCOMPARE(design.get<std::string>("scaner"), std::string(""));
    QCOMPARE(design.get<std::string>("hold"), std::string("1"));
    QCOMPARE(design.get<std::string>("people_capacity"), std::string("0"));
    QCOMPARE(design.get<std::string>("magazines"), std::string("0"));
    QCOMPARE(design.get<std::string>("transit_mod"), std::string("5"));
    QCOMPARE(design.get<std::string>("speed.turn_mode"), std::string("2"));
    QCOMPARE(design.get<std::string>("speed.cruise"), std::string("4"));
    QCOMPARE(design.get<std::string>("speed.battle"), std::string("2"));
    QCOMPARE(design.get<std::string>("components"), std::string("HXeQa(Ic)"));
  }
  catch (boost::property_tree::ptree_bad_path const &ex)
  {
    QVERIFY2(false, QString("Incorrect path: %1").arg(ex.what()).toLatin1());
    return;
  }
}

void
GameTurnToPropertyTreeExporterTest::test_file_with_0_fleets_orders()
{
  // GIVEN
  SFGameData data;

  // WHEN
  boost::property_tree::ptree dataTree = test_object->ExportToPropertyTree(data);
  // THEN

  try
  {
    boost::property_tree::ptree fleetsOrders = dataTree.get_child("StarFire_Turn.fleets");
    QVERIFY(!fleetsOrders.size());
  }
  catch (boost::property_tree::ptree_bad_path const &ex)
  {
    QVERIFY2(false, QString("Incorrect path: %1").arg(ex.what()).toLatin1());
    return;
  }
}

void
GameTurnToPropertyTreeExporterTest::test_file_with_1_fleets_orders()
{
  // GIVEN
  SFGameData  data;
  FleetOrders orders;
  orders.name            = "1.Fleet";
  orders.leaderName      = "Leader";
  orders.speed           = 4;
  orders.currentPosition = "Rigel";
  orders.orders.push_back("Move to Kontar");
  orders.orders.push_back("Move to Bedard");
  orders.orders.push_back("Move to Feros Star");
  orders.orders.push_back("");
  data.fleetsOrders[orders.name] = orders;

  // WHEN
  boost::property_tree::ptree dataTree = test_object->ExportToPropertyTree(data);
  // THEN
  try
  {
    ptree fleets_orders_tree = dataTree.get_child("StarFire_Turn.fleets");
    QVERIFY(fleets_orders_tree.size());
    ptree::iterator iter              = fleets_orders_tree.begin();
    ptree           single_fleet_tree = iter->second;
    QCOMPARE(single_fleet_tree.get<std::string>("name"), std::string("1.Fleet"));
    QCOMPARE(single_fleet_tree.get<std::string>("leader_name"), std::string("Leader"));
    QCOMPARE(single_fleet_tree.get<unsigned int>("speed"), 4u);
    QCOMPARE(single_fleet_tree.get<std::string>("position"), std::string("Rigel"));

    auto fleet_orders_tree = single_fleet_tree.get_child("orders");
    iter                   = fleet_orders_tree.begin();
    QCOMPARE(iter->first, std::string("order"));
    QCOMPARE(iter->second.data(), orders.orders[0]);
    iter++;
    QCOMPARE(iter->first, std::string("order"));
    QCOMPARE(iter->second.data(), orders.orders[1]);
    iter++;
    QCOMPARE(iter->first, std::string("order"));
    QCOMPARE(iter->second.data(), orders.orders[2]);
    iter++;
    QCOMPARE(iter->first, std::string("order"));
    QCOMPARE(iter->second.data(), orders.orders[3]);
  }
  catch (boost::property_tree::ptree_bad_path const &ex)
  {
    QVERIFY2(false, QString("Incorrect path: %1").arg(ex.what()).toLatin1());
    return;
  }
}

void
GameTurnToPropertyTreeExporterTest::test_file_with_2_fleets_orders()
{
  // GIVEN
  SFGameData  data;
  FleetOrders orders;
  orders.name            = "1.Fleet";
  orders.leaderName      = "Leader";
  orders.speed           = 4;
  orders.currentPosition = "Rigel";
  orders.orders.push_back("Move to Kontar");
  orders.orders.push_back("Move to Bedard");
  orders.orders.push_back("Move to Feros Star");
  orders.orders.push_back("");
  data.fleetsOrders[orders.name] = orders;

  FleetOrders order2;
  order2.name            = "WarpSurvey";
  order2.leaderName      = "";
  order2.speed           = 4;
  order2.currentPosition = "Kontar";
  order2.orders.push_back("Survey Rigel Conceal");
  order2.orders.push_back("Survey Rigel Conceal");
  order2.orders.push_back("Survey Rigel Conceal");
  order2.orders.push_back("Survey Rigel Conceal");
  data.fleetsOrders[order2.name] = order2;
  // WHEN
  boost::property_tree::ptree dataTree = test_object->ExportToPropertyTree(data);
  // THEN
  try
  {
    ptree fleets_orders_tree = dataTree.get_child("StarFire_Turn.fleets");
    QVERIFY(fleets_orders_tree.size());
    ptree::iterator iter = fleets_orders_tree.begin();
    iter++;
    ptree single_fleet_tree = iter->second;
    QCOMPARE(single_fleet_tree.get<std::string>("name"), std::string("WarpSurvey"));
    QCOMPARE(single_fleet_tree.get<std::string>("leader_name"), std::string(""));
    QCOMPARE(single_fleet_tree.get<unsigned int>("speed"), 4u);
    QCOMPARE(single_fleet_tree.get<std::string>("position"), std::string("Kontar"));

    auto fleet_orders_tree = single_fleet_tree.get_child("orders");
    iter                   = fleet_orders_tree.begin();
    QCOMPARE(iter->first, std::string("order"));
    QCOMPARE(iter->second.data(), order2.orders[0]);
    iter++;
    QCOMPARE(iter->first, std::string("order"));
    QCOMPARE(iter->second.data(), order2.orders[1]);
    iter++;
    QCOMPARE(iter->first, std::string("order"));
    QCOMPARE(iter->second.data(), order2.orders[2]);
    iter++;
    QCOMPARE(iter->first, std::string("order"));
    QCOMPARE(iter->second.data(), order2.orders[3]);
  }
  catch (boost::property_tree::ptree_bad_path const &ex)
  {
    QVERIFY2(false, QString("Incorrect path: %1").arg(ex.what()).toLatin1());
    return;
  }
}

void
GameTurnToPropertyTreeExporterTest::test_0_fleets_details()
{
  // GIVEN
  SFGameData  data;
  FleetOrders orders;
  orders.name                    = "1.Fleet";
  orders.leaderName              = "Leader";
  orders.speed                   = 4;
  orders.currentPosition         = "Rigel";
  orders.orders                  = {"Move to Kontar", "Move to bedard", "Move to Feros Star", ""};
  data.fleetsOrders[orders.name] = orders;  // WHEN

  boost::property_tree::ptree dataTree = test_object->ExportToPropertyTree(data);

  try
  {
    boost::property_tree::ptree fleetsOrders = dataTree.get_child("StarFire_Turn.fleets.fleet.ships");
    QVERIFY(fleetsOrders.empty());
  }
  catch (boost::property_tree::ptree_bad_path const &ex)
  {
    QVERIFY2(false, QString("Incorrect path: %1").arg(ex.what()).toLatin1());
    return;
  }
}

void
GameTurnToPropertyTreeExporterTest::test_1_fleets_details()
{
  // GIVEN
  SFGameData         data;
  FleetOrders        orders = generateFleetOrders(4);
  Ships::ShipDetails ship   = generateShipDetails();
  data.fleetsComposition[orders.name].push_back(ship);
  data.fleetsOrders[orders.name] = orders;

  // WHEN
  boost::property_tree::ptree dataTree = test_object->ExportToPropertyTree(data);
  // THEN
  try
  {
    boost::property_tree::ptree ships_tree = dataTree.get_child("StarFire_Turn.fleets.fleet.ships");
    QCOMPARE(ships_tree.size(), 1ul);
    auto single_ship_tree = ships_tree.begin()->second;
    QCOMPARE(single_ship_tree.get<std::string>("class"), ship.className);
    QCOMPARE(single_ship_tree.get<unsigned int>("amount"), ship.amount);
    auto crew_grade_tree = single_ship_tree.get_child("crew_grade");
    QCOMPARE(crew_grade_tree.size(), 2ul);
    auto grade_tree_iter = crew_grade_tree.begin();
    QCOMPARE(grade_tree_iter->second.get<int>("<xmlattr>.level"), static_cast<int>(Ships::bg_Average));
    QCOMPARE(grade_tree_iter->second.data(), std::to_string(ship.crewGrade[Ships::bg_Average]));
    grade_tree_iter++;
    QCOMPARE(grade_tree_iter->second.get<int>("<xmlattr>.level"), static_cast<int>(Ships::bg_Crack));
    QCOMPARE(grade_tree_iter->second.data(), std::to_string(ship.crewGrade[Ships::bg_Crack]));
    QCOMPARE(single_ship_tree.get<unsigned int>("speed.tact"), ship.speedTactical);
    QCOMPARE(single_ship_tree.get<unsigned int>("speed.strat"), ship.speedStategic);
    QCOMPARE(single_ship_tree.get<unsigned int>("speed.lod"), ship.levelOfDetection);
    QCOMPARE(single_ship_tree.get<unsigned int>("speed.clk"), ship.levelCloak);
  }
  catch (boost::property_tree::ptree_bad_path const &ex)
  {
    QVERIFY2(false, QString("Incorrect path: %1").arg(ex.what()).toLatin1());
    return;
  }
}

void
GameTurnToPropertyTreeExporterTest::test_2_fleets_details()
{
  // GIVEN
  SFGameData         data;
  FleetOrders        orders = generateFleetOrders(4);
  Ships::ShipDetails ship   = generateShipDetails();
  data.fleetsComposition[orders.name].push_back(ship);
  data.fleetsOrders[orders.name] = orders;

  ship   = generateShipDetails();
  orders = generateFleetOrders(6);
  data.fleetsComposition[orders.name].push_back(ship);
  data.fleetsOrders[orders.name] = orders;

  boost::property_tree::ptree dataTree = test_object->ExportToPropertyTree(data);

  try
  {
    boost::property_tree::ptree ships_tree = dataTree.get_child("StarFire_Turn.fleets.fleet.ships");
    QCOMPARE(ships_tree.size(), 1ul);
    auto single_ship_tree = ships_tree.begin()->second;
    QCOMPARE(single_ship_tree.get<std::string>("class"), ship.className);
    QCOMPARE(single_ship_tree.get<unsigned int>("amount"), ship.amount);
    auto crew_grade_tree = single_ship_tree.get_child("crew_grade");
    QCOMPARE(crew_grade_tree.size(), 2ul);
    auto grade_tree_iter = crew_grade_tree.begin();
    QCOMPARE(grade_tree_iter->second.get<int>("<xmlattr>.level"), static_cast<int>(Ships::bg_Average));
    QCOMPARE(grade_tree_iter->second.data(), std::to_string(ship.crewGrade[Ships::bg_Average]));
    grade_tree_iter++;
    QCOMPARE(grade_tree_iter->second.get<int>("<xmlattr>.level"), static_cast<int>(Ships::bg_Crack));
    QCOMPARE(grade_tree_iter->second.data(), std::to_string(ship.crewGrade[Ships::bg_Crack]));
    QCOMPARE(single_ship_tree.get<unsigned int>("speed.tact"), ship.speedTactical);
    QCOMPARE(single_ship_tree.get<unsigned int>("speed.strat"), ship.speedStategic);
    QCOMPARE(single_ship_tree.get<unsigned int>("speed.lod"), ship.levelOfDetection);
    QCOMPARE(single_ship_tree.get<unsigned int>("speed.clk"), ship.levelCloak);
  }
  catch (boost::property_tree::ptree_bad_path const &ex)
  {
    QVERIFY2(false, QString("Incorrect path: %1").arg(ex.what()).toLatin1());
    return;
  }
}
