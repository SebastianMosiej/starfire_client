#include "game_turn_to_property_tree_exporter.h"
#include <boost/property_tree/xml_parser.hpp>
#include <boost/filesystem.hpp>

namespace {
  namespace fs = boost::filesystem;
  const fs::path kExportDirectory("game_data");
  const fs::path kExportFileName("starfire_turn.xml");
  const fs::path kExportFilePath = kExportDirectory / kExportFileName;
  const std::string kRootTag("StarFire_Turn");
  const std::string kRootXmlnsTag("<xmlattr>.xmlns");
  const std::string kXmlnsAttribVal("https://www.w3schools.com");
  const std::string kRootXmlnsXsiTag("<xmlattr>.xmlns:xsi");
  const std::string kXmlnsXsiAttribVal("http://www.w3.org/2001/XMLSchema-instance");
  const std::string kRootSchemeTag("<xmlattr>.xsi:schemaLocation");
  const std::string kSchemeLocationVal("https://www.w3schools.com turn.xsd");
}

namespace pt = boost::property_tree;

void GameTurnToPropertyTreeExporter::VerifyExportFilePresence()
{
  if (fs::is_directory(kExportDirectory))
  {
    fs::create_directories(kExportDirectory);
  }
  if (fs::is_regular_file(kExportFileName))
  {
      std::ofstream file(kExportFilePath.string(), std::ios::out | std::ios::trunc);
      file.close();
  }
}

void GameTurnToPropertyTreeExporter::AddShipsProjects(const SFGameData &data, pt::ptree &turn_tree)
{
  pt::ptree & projects_tree = turn_tree.add("projects", "");
  for(auto pair_ : data.shipProjects)
  {
     auto singleProject = pair_.second;
     pt::ptree project;
     project.add("name", singleProject.className);
     project.add("hull_type", singleProject.hullType);
     project.add("hs_size", singleProject.hullSize);
     project.add("cost", singleProject.cost);
     project.add<float>("maintenance", singleProject.maintenance);
     project.add("HTK", singleProject.hit_to_kill);
     project.add("datalinks", singleProject.datalink);
     project.add("multitarget", singleProject.multitarget);
     project.add("survey_pts", singleProject.surveyPoints);
     project.add("scaner", singleProject.sensorType);
     project.add("hold", singleProject.cargoCapacity);
     project.add("people_capacity", singleProject.peopleCapacity);
     project.add("magazines", singleProject.magazineCapacity);
     project.add("transit_mod", singleProject.transitModifier);
     project.add("speed.turn_mode", singleProject.turnMode);
     project.add("speed.cruise", singleProject.strategicSpeed);
     project.add("speed.battle", singleProject.tacticalSpeed);
     project.add("components", singleProject.design);

     projects_tree.add_child("project", project);
  }
}

void GameTurnToPropertyTreeExporter::AddFleets(const SFGameData &data, pt::ptree &turn_tree)
{
  std::string fleet_tag("fleet");
  pt::ptree & fleets_tree = turn_tree.add("fleets", "");
  for(auto pair_ : data.fleetsOrders)
  {
    auto fleet_orders_data = pair_.second;
    pt::ptree fleet_orders_tree;
    fleet_orders_tree.add("name", fleet_orders_data.name);
    fleet_orders_tree.add("leader_name", fleet_orders_data.leaderName);
    fleet_orders_tree.add("speed", fleet_orders_data.speed);
    fleet_orders_tree.add("position", fleet_orders_data.currentPosition);
    pt::ptree &orders_tree = fleet_orders_tree.add("orders","");
    for(const std::string &singleOrder : fleet_orders_data.orders)
    {
      orders_tree.add("order", singleOrder);
    }
    pt::ptree & ships_tree = fleet_orders_tree.add("ships", "");
    if (data.fleetsComposition.count(fleet_orders_data.name))
    {
       const auto &fleetComposition = data.fleetsComposition.at(fleet_orders_data.name);
      AddShipDetails(fleetComposition, ships_tree);
    }

    fleets_tree.add_child(fleet_tag, fleet_orders_tree);
  }
}

void GameTurnToPropertyTreeExporter::AddShipDetails(const std::vector<Ships::ShipDetails> &data, pt::ptree &ships_tree)
{
  for(const auto & shipData : data)
  {
    pt::ptree ship_tree;
    ship_tree.add("class", shipData.className);
    ship_tree.add<unsigned int>("amount", shipData.amount);
    pt::ptree crew_grade_tree;
    for(const auto &crewGradeData : shipData.crewGrade)
    {
       pt::ptree grade_tree(std::to_string(crewGradeData.second));
       grade_tree.add("<xmlattr>.level", crewGradeData.first);
       crew_grade_tree.add_child("grade", grade_tree);
    }
    ship_tree.add_child("crew_grade", crew_grade_tree);
    pt::ptree speed_tree;
    speed_tree.add("tact", shipData.speedTactical);
    speed_tree.add("strat", shipData.speedStategic);
    speed_tree.add("lod", shipData.levelOfDetection);
    speed_tree.add("clk", shipData.levelCloak);
    ship_tree.add_child("speed", speed_tree);

    ships_tree.add_child("ship", ship_tree);
  }
}

pt::ptree GameTurnToPropertyTreeExporter::ExportToPropertyTree(const SFGameData &data)
{
  pt::ptree root_tree;
  VerifyExportFilePresence();

  root_tree.add(kRootTag, "" );
  {
    pt::ptree & turn_tree = root_tree.get_child(kRootTag);
    turn_tree.add(kRootXmlnsTag, kXmlnsAttribVal);
    turn_tree.add(kRootXmlnsXsiTag, kXmlnsXsiAttribVal);
    turn_tree.add(kRootSchemeTag, kSchemeLocationVal);

    AddShipsProjects(data, turn_tree);
    AddFleets(data, turn_tree);
  }
  return std::move(root_tree);
}

void GameTurnToPropertyTreeExporter::SaveXmlFile(const pt::ptree &root_tree) const
{
  try {
    boost::property_tree::xml_parser::xml_writer_settings<std::__cxx11::string> settings(' ', 2);
    write_xml(kExportFilePath.string().c_str(), root_tree, std::locale(), settings);
  } catch (boost::property_tree::xml_parser::xml_parser_error const & ex) {
    std::string str;
    str = ex.message();
    str = "";
//    logger.log(log4cplus::ERROR_LOG_LEVEL, QString("Error during properties file parsing ('%1')").arg(ex.message().c_str()).toStdString().c_str());
    return;
  }
}
