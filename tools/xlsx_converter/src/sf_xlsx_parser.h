#pragma once
#include <data/turn_data/fleet_detail.hpp>
#include <data/turn_data/ship_project.hpp>
#include <data/turn_data/planet_detail.hpp>
#include <memory>
#include <vector>

class SFXlsxParserPrivate;

class SFXlsxParser {
 public:
  SFXlsxParser(const std::string &path);
  ~SFXlsxParser();
  std::vector<FleetOrders> get_fleet_orders();
  std::vector<Fleet> get_fleets_composition();
  std::vector<Ships::ShipProject> get_ship_projects();
  std::vector<Planet::Planets> get_planets();

 private:
  friend class SFXlsxParserPrivate;
  std::unique_ptr<SFXlsxParserPrivate> pimpl;
};
