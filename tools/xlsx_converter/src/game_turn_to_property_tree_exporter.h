#pragma once
#include <data/turn_data/sf_game.hpp>
#include <boost/property_tree/ptree.hpp>

class GameTurnToPropertyTreeExporter
{
public:
  boost::property_tree::ptree ExportToPropertyTree(const SFGameData &data);
private:
  void VerifyExportFilePresence();
  void AddShipsProjects(const SFGameData &data, boost::property_tree::ptree &turn_tree);
  void AddFleets(const SFGameData &data, boost::property_tree::ptree &turn_tree);
  void AddShipDetails(const std::vector<Ships::ShipDetails> &data, boost::property_tree::ptree &ships_tree);
  void SaveXmlFile(const boost::property_tree::ptree &root_tree) const;
};
