#include <fleets_xlsx_parser.h>
#include <sf_xlsx_parser.h>
#include <xlsxabstractsheet.h>
#include <xlsxcell.h>
#include <xlsxdocument.h>
#include <xlsxworksheet.h>
#include <string>

namespace
{
  const unsigned int FleetsCompositionInitialRow(8);
  const unsigned int FleetsCompositionFinalRow(45);
  const unsigned int FleetsCompositionInitialGradeColumn(8);
  const unsigned int FleetsCompositionFinalGradeColumn(15);
  const unsigned int ProjectInitialRow(8);
  const unsigned int ProjectFinalRow(100);

  const unsigned int PlanetsInitialRow(7);
  const unsigned int PlanetsFinalRow(133);
}

class SFXlsxParserPrivate
{
 public:
  SFXlsxParserPrivate(SFXlsxParser *_pub, const std::string &path);
  std::vector<FleetOrders>        get_fleet_orders();
  std::vector<Fleet>              get_fleets_composition();
  std::vector<Ships::ShipProject> get_ship_projects();
  std::vector<Planet::Planets>    get_planets();
  Ships::ShipProject              parse_ship_project(QXlsx::Worksheet *sheet, int row);
  Planet::Planets                 parse_planet_data(QXlsx::Worksheet *sheet, int row);
  void               parse_fleet_composition(QXlsx::Worksheet *sheet, int row, std::map<std::string, Fleet> &fleetsMap);
  Ships::ShipDetails parse_fleet_ship_details(QXlsx::Worksheet *sheet, int row);
  void parse_planet_position_data(Planet::Planets & planet, std::string cell);
  void parse_planet_habitability(Planet::Planets &planet, std::string cell);
  void parser_planet_bodytype(Planet::Planets &planetData, std::string cellString);
  void parser_planet_buildings(Planet::Planets &planetData, QXlsx::Worksheet *sheet, int row);

 public:
  SFXlsxParser *           pub;
  QXlsx::Document          xlsx;
  Fleets::FleetsXlsxParser fleetOrdersParser;

 private:
  void parse_ship_speed(Ships::ShipProject &project, std::string cell);
  void addFleetIfNotExist(const std::string &str, std::map<std::string, Fleet> &fleetsMap);
};

SFXlsxParserPrivate::SFXlsxParserPrivate(SFXlsxParser *_pub, const std::string &path)
    : pub(_pub)
    , xlsx(path.c_str())
    , fleetOrdersParser(xlsx)
{
}

void
SFXlsxParserPrivate::addFleetIfNotExist(const std::string &str, std::map<std::string, Fleet> &fleetsMap)
{
  if (fleetsMap.find(str) == fleetsMap.end()) {
    Fleet fleet;
    fleet.name = str;
    fleetsMap.insert(std::make_pair(str, fleet));
  }
}

Ships::ShipDetails
SFXlsxParserPrivate::parse_fleet_ship_details(QXlsx::Worksheet *sheet, int row)
{
  Ships::ShipDetails details;
  std::string        str = sheet->cellAt(row, 4)->value().toByteArray().toStdString();
  details.className      = str;
  details.amount         = sheet->cellAt(row, 7)->value().toUInt();
  for (int gradeColumn = FleetsCompositionInitialGradeColumn; gradeColumn <= FleetsCompositionFinalGradeColumn;
       gradeColumn++)
  {
    unsigned int amount = sheet->cellAt(row, gradeColumn)->value().toUInt();
    if (amount == 0) continue;
    Ships::BaseGrade grade =
        static_cast<Ships::BaseGrade>(gradeColumn - FleetsCompositionInitialGradeColumn + Ships::bg_Poor);
    details.crewGrade.insert(std::make_pair(grade, amount));
  }
  details.speedTactical    = sheet->cellAt(row, 16)->value().toUInt();
  details.speedStategic    = sheet->cellAt(row, 17)->value().toUInt();
  details.levelOfDetection = sheet->cellAt(row, 18)->value().toUInt();
  details.levelCloak       = sheet->cellAt(row, 19)->value().toUInt();
  details.mothball         = sheet->cellAt(row, 25)->value().toBool();
  return details;
}

void
SFXlsxParserPrivate::parse_fleet_composition(QXlsx::Worksheet *sheet, int row, std::map<std::string, Fleet> &fleetsMap)
{
  std::string str = sheet->cellAt(row, 1)->value().toByteArray().toStdString();
  if (str.empty()) return;
  addFleetIfNotExist(str, fleetsMap);
  Fleet &            fleet   = fleetsMap[str];
  Ships::ShipDetails details = parse_fleet_ship_details(sheet, row);
  fleet.ships.push_back(details);
}

std::vector<Fleet>
SFXlsxParserPrivate::get_fleets_composition()
{
  std::map<std::string, Fleet> fleetsMap;
  auto *                       fleetShips = xlsx.sheet("Ships");
  if (!fleetShips) return std::vector<Fleet>();

  if (fleetShips->sheetType() == QXlsx::AbstractSheet::ST_WorkSheet) {
    auto *sheet = dynamic_cast<QXlsx::Worksheet *>(fleetShips);
    for (int row = FleetsCompositionInitialRow; row != FleetsCompositionFinalRow; row++) {
      parse_fleet_composition(sheet, row, fleetsMap);
    }
  }

  std::vector<Fleet> fleetsComposition;
  for (std::map<std::string, Fleet>::iterator it = fleetsMap.begin(); it != fleetsMap.end(); it++)
    fleetsComposition.push_back(it->second);

  return fleetsComposition;
}

void
SFXlsxParserPrivate::parse_ship_speed(Ships::ShipProject &project, std::string cell)
{
  if (cell != "-") {
    size_t pos1            = cell.find_first_not_of("[");
    size_t pos2            = cell.find_first_of("]", pos1);
    project.turnMode       = std::stoul(cell.substr(pos1, pos2 - pos1));
    pos1                   = cell.find_first_of("[", pos2) + 1;
    pos2                   = cell.find_first_of("/", pos1);
    project.strategicSpeed = std::stoul(cell.substr(pos1, pos2 - pos1));

    pos2++;
    pos1                  = cell.find_first_of("]", pos2);
    project.tacticalSpeed = std::stoul(cell.substr(pos2, pos1 - pos2));
  }
}

Ships::ShipProject
SFXlsxParserPrivate::parse_ship_project(QXlsx::Worksheet *sheet, int row)
{
  Ships::ShipProject project;
  std::string        str   = sheet->cellAt(row, 1)->value().toByteArray().toStdString();
  project.className        = str;
  str                      = sheet->cellAt(row, 3)->value().toByteArray().toStdString();
  project.hullType         = str;
  project.hullSize         = sheet->cellAt(row, 4)->value().toUInt();
  project.cost             = sheet->cellAt(row, 5)->value().toFloat();
  project.maintenance      = sheet->cellAt(row, 7)->value().toFloat();
  project.eqList           = sheet->cellAt(row, 9)->value().toByteArray().toStdString();
  project.hit_to_kill      = sheet->cellAt(row, 12)->value().toUInt();
  project.datalink         = sheet->cellAt(row, 13)->value().toByteArray().toStdString();
  project.multitarget      = sheet->cellAt(row, 14)->value().toUInt();
  project.surveyPoints     = sheet->cellAt(row, 15)->value().toUInt();
  project.sensorType       = sheet->cellAt(row, 16)->value().toByteArray().toStdString();
  project.cargoCapacity    = sheet->cellAt(row, 17)->value().toUInt();
  project.peopleCapacity   = sheet->cellAt(row, 18)->value().toUInt();
  project.magazineCapacity = sheet->cellAt(row, 19)->value().toUInt();
  project.transitModifier  = sheet->cellAt(row, 20)->value().toUInt();
  str                      = sheet->cellAt(row, 21)->value().toByteArray().toStdString();
  parse_ship_speed(project, str);
  project.design = sheet->cellAt(row, 23)->value().toByteArray().toStdString();
  return project;
}

std::vector<Ships::ShipProject>
SFXlsxParserPrivate::get_ship_projects()
{
  std::vector<Ships::ShipProject> projects;

  auto *fleetShips = xlsx.sheet("Pro");
  if (!fleetShips) return projects;

  if (fleetShips->sheetType() == QXlsx::AbstractSheet::ST_WorkSheet) {
    auto *sheet = dynamic_cast<QXlsx::Worksheet *>(fleetShips);
    for (int row = ProjectInitialRow; row != ProjectFinalRow; row++) {
      if (sheet->cellAt(row, 1)->value().toByteArray().toStdString().empty()) continue;
      projects.push_back(parse_ship_project(sheet, row));
    }
  }

  return projects;
}

void SFXlsxParserPrivate::parse_planet_position_data(Planet::Planets & planet, std::string cell) {
  std::size_t slashPos = cell.find("/");
  if (slashPos != std::string::npos) {
    std::string star = cell.substr(0, slashPos);
    if (star == "A")
      planet.starNumber = 1;
    else if (star == "B")
      planet.starNumber = 2;
    cell = cell.substr(slashPos + 1);
  }
  std::size_t dotPos = cell.find(".");
  if (dotPos != std::string::npos) {
    std::string moon = cell.substr(dotPos+1);
    planet.moonNumber = QString(moon.c_str()).toUInt();
    cell = cell.substr(0, dotPos);
  }
  planet.planetNumber = QString(cell.c_str()).toUInt();
}

void SFXlsxParserPrivate::parse_planet_habitability(Planet::Planets &planet, std::string cell)
{
  std::size_t dotPos = cell.find("(");
  cell = cell.substr(0, dotPos);
  if (cell == "B")
    planet.habitability = +Planet::WorldType::Bening;
  else if (cell == "De")
    planet.habitability = +Planet::WorldType::Desolate;
}

void SFXlsxParserPrivate::parser_planet_bodytype(Planet::Planets &planet, std::string cell)
{
  std::size_t plusPos = cell.find("+");
  cell = cell.substr(0, plusPos);
  planet.bodyType = Planet::BodyType::_from_string(cell.c_str());
}

void SFXlsxParserPrivate::parser_planet_buildings(Planet::Planets &planetData, QXlsx::Worksheet *sheet, int row)
{
  unsigned int buildingsAmount = sheet->cellAt(row,18)->value().toByteArray().toUInt();
  planetData.buildings[Planet::BuildingsType::ScienceAcademy] = buildingsAmount;
  buildingsAmount = sheet->cellAt(row,19)->value().toByteArray().toUInt();
  planetData.buildings[Planet::BuildingsType::ResearchDevelopmentStation] = buildingsAmount;
  buildingsAmount = sheet->cellAt(row,20)->value().toByteArray().toUInt();
  planetData.buildings[Planet::BuildingsType::PilotTrainingFacility] = buildingsAmount;
  buildingsAmount = sheet->cellAt(row,21)->value().toByteArray().toUInt();
  planetData.buildings[Planet::BuildingsType::QtTrainingFacility] = buildingsAmount;
  buildingsAmount = sheet->cellAt(row,22)->value().toByteArray().toUInt();
  planetData.buildings[Planet::BuildingsType::PlanetaryScienceFacility] = buildingsAmount;
  buildingsAmount = sheet->cellAt(row,23)->value().toByteArray().toUInt();
  planetData.buildings[Planet::BuildingsType::TradeHub] = buildingsAmount;
  buildingsAmount = sheet->cellAt(row,24)->value().toByteArray().toUInt();
  planetData.buildings[Planet::BuildingsType::ImperialCommandCenter] = buildingsAmount;
  buildingsAmount = sheet->cellAt(row,25)->value().toByteArray().toUInt();
  planetData.buildings[Planet::BuildingsType::ImperialCapital] = buildingsAmount;
}

Planet::Planets SFXlsxParserPrivate::parse_planet_data(QXlsx::Worksheet *sheet, int row)
{
  Planet::Planets planetData;
  planetData.systemName = sheet->cellAt(row, 1)->value().toByteArray().toStdString();

  std::string cellString = sheet->cellAt(row,3)->value().toByteArray().toStdString();
  parse_planet_position_data(planetData, cellString);
  cellString = sheet->cellAt(row,4)->value().toByteArray().toStdString();
  parse_planet_habitability(planetData, cellString);
  cellString = sheet->cellAt(row,5)->value().toByteArray().toStdString();
  parser_planet_bodytype(planetData, cellString);
  planetData.population = sheet->cellAt(row,6)->value().toDouble();
  planetData.growth = sheet->cellAt(row,7)->value().toDouble();
  planetData.maximumPopulation = sheet->cellAt(row,10)->value().toUInt();
  planetData.industrialUnit = sheet->cellAt(row,12)->value().toUInt();
  planetData.resourceExploitationIndex = sheet->cellAt(row,15)->value().toDouble()*100;
  planetData.income = sheet->cellAt(row,16)->value().toDouble();
  parser_planet_buildings(planetData, sheet, row);

  return planetData;
}
std::vector<Planet::Planets> SFXlsxParserPrivate::get_planets()
{
  std::vector<Planet::Planets> result;
  auto *planetsPage = xlsx.sheet("PU");
  if (!planetsPage) return result;
  if (planetsPage->sheetType() == QXlsx::AbstractSheet::ST_WorkSheet) {
    auto *sheet = dynamic_cast<QXlsx::Worksheet *>(planetsPage);
    for(int row = PlanetsInitialRow; row != PlanetsFinalRow; row++)
    {
      if (sheet->cellAt(row, 1)->value().isNull()) continue;
      result.push_back(parse_planet_data(sheet, row));
    }
  }

  return result;
}

////////////////////////////////////////////////////////////////////////////////
SFXlsxParser::SFXlsxParser(const std::string &path)
    : pimpl(new SFXlsxParserPrivate(this, path))
{
}

SFXlsxParser::~SFXlsxParser()
{
}

std::vector<FleetOrders>
SFXlsxParser::get_fleet_orders()
{
  return pimpl->fleetOrdersParser.get_fleet_orders();
}
std::vector<Fleet>
SFXlsxParser::get_fleets_composition()
{
  return pimpl->get_fleets_composition();
}

std::vector<Ships::ShipProject>
SFXlsxParser::get_ship_projects()
{
  return pimpl->get_ship_projects();
}

std::vector<Planet::Planets> SFXlsxParser::get_planets()
{
  return pimpl->get_planets();
}
