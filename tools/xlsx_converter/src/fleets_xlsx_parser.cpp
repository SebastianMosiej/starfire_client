#include <fleets_xlsx_parser.h>
#include <xlsxdocument.h>
#include <xlsxabstractsheet.h>
#include <xlsxworksheet.h>
#include <xlsxcell.h>

namespace Fleets {
enum Columns {
  col_FleetName = 1,
  col_Leader = 4,
  col_Status = 11,
  col_Speed = 15,
  col_FirstOrder = 17,
  col_Position = 35
};

class FleetsXlsxParserPrivate {
 public:
  FleetsXlsxParserPrivate(FleetsXlsxParser *_pub, QXlsx::Document &xlsxDoc);
  std::vector<FleetOrders> get_fleet_orders();
  std::vector<std::string> get_orders(QXlsx::Worksheet * sheet, int row);

 public:
  FleetsXlsxParser *pub;
  QXlsx::Document &xlsxDoc;
  
};

FleetsXlsxParserPrivate::FleetsXlsxParserPrivate(FleetsXlsxParser *_pub, QXlsx::Document &xlsxDoc_)
    : pub(_pub), xlsxDoc(xlsxDoc_) {}

std::vector<std::string> FleetsXlsxParserPrivate::get_orders(QXlsx::Worksheet * sheet, int row)
{
  std::vector<std::string> orders;
  for(int column = col_FirstOrder;;)
  {
    std::string str;
    str = sheet->cellAt(row, column)->value().toByteArray().toStdString();
    if (str.empty() || column >= col_Position)
      break;
    orders.push_back(str);
    column+=3;
  }

  return orders;
}

std::vector<FleetOrders> FleetsXlsxParserPrivate::get_fleet_orders()
{
  std::vector<FleetOrders> fleetsOrders;

  auto * fleetSheet =  xlsxDoc.sheet("Fleet");
  if (!fleetSheet)
    return fleetsOrders;
  int initialRow = 8;

  if (fleetSheet->sheetType() == QXlsx::AbstractSheet::ST_WorkSheet)
  {
    auto * sheet = dynamic_cast<QXlsx::Worksheet*>(fleetSheet);
    for (int row = initialRow;;row++)
    {
      std::string  str =  sheet->cellAt(row, col_FleetName)->value().toByteArray().toStdString();
      if (str.empty())
          break;
      FleetOrders orders;
      orders.name = str;
      orders.leaderName = sheet->cellAt(row, col_Leader)->value().toByteArray().toStdString();
      orders.status = sheet->cellAt(row, col_Status)->value().toByteArray().toStdString();
      orders.speed = sheet->cellAt(row, col_Speed)->value().toUInt();
      orders.currentPosition = sheet->cellAt(row, col_Position)->value().toByteArray().toStdString();
      orders.orders = get_orders(sheet, row);

      fleetsOrders.push_back(orders);
    }
  }

  return fleetsOrders;
}

////////////////////////////////////////////////////////////////////////////////
FleetsXlsxParser::FleetsXlsxParser(QXlsx::Document & xlsxDoc) : pimpl(new FleetsXlsxParserPrivate(this, xlsxDoc)) {}

FleetsXlsxParser::~FleetsXlsxParser() {}

std::vector<FleetOrders> FleetsXlsxParser::get_fleet_orders() {
  std::vector<FleetOrders> orders(pimpl->get_fleet_orders());
  return orders;
}
}
