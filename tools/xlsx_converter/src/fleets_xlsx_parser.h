#pragma once
#include <data/turn_data/fleet_detail.hpp>
#include <xlsxdocument.h>
#include <memory>
#include <vector>

namespace Fleets {

class FleetsXlsxParserPrivate;

class FleetsXlsxParser {
 public:
  FleetsXlsxParser(QXlsx::Document & xlsxDoc);
  ~FleetsXlsxParser();
  std::vector<FleetOrders> get_fleet_orders();

 private:
  friend class FleetsXlsxParserPrivate;
  std::unique_ptr<FleetsXlsxParserPrivate> pimpl;
};
}
