#include <QtTest/QtTest>
#include <TestRunner.h>
#include <memory>
#include <load_pdf_document.h>

class LoadPdfDocumentTest : public QObject
{
    Q_OBJECT
public:
    std::shared_ptr<LoadPdfDocument> test_object;

private Q_SLOTS:
    void init();
    void cleanup();
    void test_loadPdfDocument();
};

DECLARE_TEST(LoadPdfDocumentTest)
