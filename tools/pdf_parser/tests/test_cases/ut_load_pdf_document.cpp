#include "ut_load_pdf_document.h"
#include <i_engine_tech.h>

namespace {
const std::string pdf_file("Solar_StarFire-201307.pdf");
}

void LoadPdfDocumentTest::init()
{
  test_object.reset(new LoadPdfDocument());
}

void LoadPdfDocumentTest::cleanup()
{
  test_object.reset();
}

void LoadPdfDocumentTest::test_loadPdfDocument()
{
  QVERIFY(test_object->loadDocument(pdf_file));
  QCOMPARE(test_object->getDocumentTitle(), std::string("Solar Starfire"));
  QCOMPARE(test_object->getPageCount(), 429u);
  std::vector<i_engine_tech> tech = test_object->getInertialessEngineTable();
  QCOMPARE(tech.size(), static_cast<size_t>(23));
}
