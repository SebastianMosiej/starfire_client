#include <string>
#include <memory>
#include <vector>
#include <i_engine_tech.h>

class LoadPdfDocumentPrivate;

class LoadPdfDocument
{
public:
  LoadPdfDocument();
  ~LoadPdfDocument();
  bool loadDocument(std::string path);
  std::string getDocumentTitle();
  unsigned int getPageCount();
  std::vector<i_engine_tech> getInertialessEngineTable();
private:
  std::unique_ptr<LoadPdfDocumentPrivate> pimpl;
};
