#include "load_pdf_document.h"
#include <PdfContentsTokenizer.h>
#include <PdfMemDocument.h>
#include <logging_base.h>
#include <podofo.h>
#include <boost/format.hpp>

class LoadPdfDocumentPrivate : public LoggingBase {
 public:
  LoadPdfDocumentPrivate(LoadPdfDocument* _pub) : pub(_pub) {}
  bool loadDocument(const std::string& path);
  std::string getDocumentTitle();
  unsigned int getPageCount();
  std::vector<i_engine_tech> getInertialessEngineTable();

 public:
  LoadPdfDocument* pub;
  PoDoFo::PdfMemDocument pdfDoc;
};

bool LoadPdfDocumentPrivate::loadDocument(const std::string& path) {
  pdfDoc.Load(path.c_str());
  return pdfDoc.IsLoaded();
}

std::string LoadPdfDocumentPrivate::getDocumentTitle() {
  if (pdfDoc.IsLoaded()) {
    PoDoFo::PdfInfo* pdfInfo(pdfDoc.GetInfo());
    return pdfInfo->GetTitle().GetStringUtf8();
  }
  return "";
}

unsigned int LoadPdfDocumentPrivate::getPageCount() {
  if (pdfDoc.IsLoaded()) {
    return pdfDoc.GetPageCount();
  }
  return 0;
}

std::vector<i_engine_tech> LoadPdfDocumentPrivate::getInertialessEngineTable() {
  std::vector<i_engine_tech> tech;
  if (!pdfDoc.IsLoaded()) return tech;

  PoDoFo::PdfPage* page = pdfDoc.GetPage(262);
  PoDoFo::PdfContentsTokenizer tok(page);
  const char* token = nullptr;
  PoDoFo::PdfVariant var;
  PoDoFo::EPdfContentsType type;
  std::list<std::string> parameters;
  int intend_level = 0;
  int intend_step = 2;
  while (tok.ReadNext(type, token, var)) {
    if (!strcmp(token, "ET") || !strcmp(token, "EMC"))
    {
      intend_level--;
    }
    std::string intendation(intend_step*intend_level,' ');
    logger.log(log4cplus::DEBUG_LOG_LEVEL,
              str(boost::format("%1%Token found '%2%'") % intendation % token));
    if (type == PoDoFo::ePdfContentsType_Keyword) {
      if (!strcmp(token, "BT") || !strcmp(token, "MP") || !strcmp(token, "DP") ||
          !strcmp(token, "BMC") || !strcmp(token,"BDC"))
      {
        intend_level++;
      }
      else if (!strcmp(token, "BMC"))
      {
        intend_level++;
      }
      else if (!strcmp(token, "TJ") && var.IsArray()) {
        std::string text;
        PoDoFo::PdfArray& a = var.GetArray();
        for (size_t i = 0; i < a.GetSize(); ++i)
          if (a[i].IsString()) {
            const char* s = a[i].GetString().GetString();
            text+=s;
        }
        text = str(boost::format("%1%  %2%") % intendation % text);
        logger.log(log4cplus::INFO_LOG_LEVEL, text);
      }
      else if (!strcmp(token, "Table"))
      {
        logger.log(log4cplus::INFO_LOG_LEVEL, "Table token found");
      }
      else if (!strcmp(token, "TR"))
      {
        logger.log(log4cplus::INFO_LOG_LEVEL, "TR token found");
      }
      else if (!strcmp(token, "Td"))
      {
        std::string text = str(boost::format("TD token value is %1%") % var.GetNumber());
        logger.log(log4cplus::INFO_LOG_LEVEL, text);
      }
      else if (!strcmp(token, "TD"))
      {
        if (var.IsArray())
          {
            int a=-1;
          }
        else if (var.IsDictionary())
          {
            int a=-1;
          }
        else if (var.IsString())
          {
            int a=-1;
          }
        std::string text = str(boost::format("Td token value is %1%") % 0);
        logger.log(log4cplus::INFO_LOG_LEVEL, text);
      }
      else if (!strcmp(token, "TH"))
      {
        logger.log(log4cplus::INFO_LOG_LEVEL, "TH token found");
      }

      if (!strcmp(token, "O"))
        {
          int a= 1;
        }
    }
    else if (type == PoDoFo::ePdfContentsType_Variant)
    {


    }
  }
  return tech;
}

LoadPdfDocument::LoadPdfDocument() : pimpl(new LoadPdfDocumentPrivate(this)) {}
LoadPdfDocument::~LoadPdfDocument() {}
bool LoadPdfDocument::loadDocument(std::string path) {
  return pimpl->loadDocument(path);
}

std::string LoadPdfDocument::getDocumentTitle() {
  return pimpl->getDocumentTitle();
}

unsigned int LoadPdfDocument::getPageCount() { return pimpl->getPageCount(); }

std::vector<i_engine_tech> LoadPdfDocument::getInertialessEngineTable() {
  return pimpl->getInertialessEngineTable();
}
