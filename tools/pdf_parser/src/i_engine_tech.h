#ifndef I_ENGINE_TECH_HPP
#define I_ENGINE_TECH_HPP
#include <string>

enum techType
{
  t_item,
  t_level,
  t_twig,
  t_branch
};

struct i_engine_tech
{
  unsigned int sl;
  techType type;
  //branch/twig/item name
  std::string name;
  unsigned int cost;
  float space;
  std::string notes;
  bool knot;
  bool critical;
  bool hazardous;

  i_engine_tech() : sl(0), type(t_item), name(""), cost(0), space(0), notes(""), knot(false), critical(false), hazardous(false)
  {

  }
};
#endif
